﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CourierDeliveryNetwork.Web.Startup))]
namespace CourierDeliveryNetwork.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
