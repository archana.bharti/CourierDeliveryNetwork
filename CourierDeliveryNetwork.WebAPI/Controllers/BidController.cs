﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.PostBidModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Bid")]
    public class BidController : ApiController
    {

        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [SecureResource]
        [HttpPost]
        [Route("placeBid")]
        public HttpResponseMessage PlaceBid(PostBidRequest model)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("PlaceBid @Amount=@Amount,@Description=@Description,@CustomerID=@CustomerID,@CurrencyCode=@CurrencyCode,@RequirementID=@RequirementID",
                               new SqlParameter("Amount", model.Amount),
                               new SqlParameter("Description", model.Description),
                               new SqlParameter("CustomerID", UserId),
                               new SqlParameter("CurrencyCode", model.CurrencyCode),
                               new SqlParameter("RequirementID", model.RequirementID));

                PostBidModelObject objPostBidModelObject = new PostBidModelObject();

                if (value>0)
                {
                    objPostBidModelObject.response.code = CommonModel.SuccessCode;
                    objPostBidModelObject.response.message = "Your Bid is placed successfully";

                }
                else
                {
                    objPostBidModelObject.response.code = CommonModel.BadRequest;
                    objPostBidModelObject.response.message = "Something is incorrect you have entred";
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objPostBidModelObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }




        [SecureResource]
        [HttpPost]
        [Route("BidList")]
        public HttpResponseMessage BidList(BidListRequestModel objBidListRequestModel)
        {
            try
            {
                BidListResponseModel objBidListResponseModel = new BidListResponseModel();
               List<BidDetails> objBidDetails= DbContext.Database.SqlQuery<BidDetails>("GetBidList @offset=@offset,@limit=@limit,@RequirementID=@RequirementID",
                               new SqlParameter("offset", objBidListRequestModel.offset),
                               new SqlParameter("limit", objBidListRequestModel.limit),
                               new SqlParameter("RequirementID", objBidListRequestModel.RequirementID)).ToList();

                foreach(var item in objBidDetails)
                {
                    BidListData objBidListData = new BidListData();
                    objBidListData.bidDetails = item;
                    objBidListData.bidDetails.currency= DbContext.Database.SqlQuery<BidListResponseCurrency>("GetCurrencyDetail @CurrencyCode=@CurrencyCode",
                                                                                    new SqlParameter("CurrencyCode", item.CurrencyCode)).FirstOrDefault();
                    objBidListData.userDetails= DbContext.Database.SqlQuery<BidListUserDetails>("GetCustomerDetailById @CustomerID=@CustomerID",
                                                                                    new SqlParameter("CustomerID", item.CustomerID)).FirstOrDefault();
                    objBidListResponseModel.response.data.Add(objBidListData);
                }
                if (objBidListResponseModel.response.data.Count > 0)
                {
                    objBidListResponseModel.response.limit = objBidListRequestModel.limit;
                    objBidListResponseModel.response.offset = objBidListRequestModel.offset;
                    objBidListResponseModel.response.total = objBidListResponseModel.response.data.Count();
                    objBidListResponseModel.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objBidListResponseModel.response.code = CommonModel.BadRequest;
                    
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objBidListResponseModel);
                return message;
            } 
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
