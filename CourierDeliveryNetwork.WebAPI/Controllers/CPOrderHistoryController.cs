﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.CPOrderHistoryModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/CPOrderHistory")]
    public class CPOrderHistoryController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

      
        [HttpPost]
        [Route("orderHistory")]
        [SecureResource]
        public HttpResponseMessage GetOrderHistory([FromBody]OrderHistoryRequestModel model)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 carrierProviderId = DbContext.Database.SqlQuery<Int64>("Select carrierProviderId from UserToken where Token={0}", token).FirstOrDefault();

                Int32 TotalOrders = DbContext.Database.SqlQuery<Int32>("Select Count(*) from OrderFinal where carrierProviderId={0}", carrierProviderId).FirstOrDefault();

                var RequestedOrders = DbContext.Database.SqlQuery<Int64>("GetAllOrders @Info=@Info,@filterBy=@filterBy,@carrierProviderId=@carrierProviderId,@datefrom=@datefrom,@dateto=@dateto,@offset=@offset,@limit=@limit",
                                               new SqlParameter("Info", "Orders"),
                                               new SqlParameter("carrierProviderId", carrierProviderId),
                                               new SqlParameter("datefrom", model.requestdate.from),
                                               new SqlParameter("dateto", model.requestdate.to),
                                               new SqlParameter("offset", model.offset),
                                               new SqlParameter("limit", model.limit),
                                               new SqlParameter("filterBy", model.filterBy)
                                               ).ToList();

                OrdersResponse Responsemodel = new OrdersResponse();

                foreach (var orderid in RequestedOrders)
                {
                    Orders obj = new Orders();

                    obj.customer= DbContext.Database.SqlQuery<Customer>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                               new SqlParameter("Orderid", orderid),
                                               new SqlParameter("TypeofInfo", "customer")
                                               ).FirstOrDefault();

                    obj.from = DbContext.Database.SqlQuery<From>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("Orderid", orderid),
                                              new SqlParameter("TypeofInfo", "pickUp")
                                              ).FirstOrDefault();

                    obj.to = DbContext.Database.SqlQuery<To>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                             new SqlParameter("Orderid", orderid),
                                             new SqlParameter("TypeofInfo", "dropOff")
                                             ).FirstOrDefault();

                    obj.orderDetails = DbContext.Database.SqlQuery<OrderDetails>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                             new SqlParameter("Orderid", orderid),
                                             new SqlParameter("TypeofInfo", "orderDetails")
                                             ).FirstOrDefault();

                    obj.paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                            new SqlParameter("Orderid", orderid),
                                            new SqlParameter("TypeofInfo", "paymentDetails")
                                            ).FirstOrDefault();

                    obj.orderDetails.currency = DbContext.Database.SqlQuery<Currency>("Get_CP_OrderHistory @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                            new SqlParameter("Orderid", orderid),
                                            new SqlParameter("TypeofInfo", "currency")
                                            ).FirstOrDefault();

                    Responsemodel.data.Add(obj);
                }
                if (Responsemodel != null)
                {
                    Responsemodel.code = Convert.ToInt32(CommonModel.SuccessCode);
                    Responsemodel.offset = Convert.ToInt32(model.offset);
                    Responsemodel.limit = Convert.ToInt32(model.limit);
                    Responsemodel.total = Convert.ToInt32(TotalOrders);
                }
                else
                {
                    Responsemodel.code = Convert.ToInt32(CommonModel.BadRequest);
                }
                var message = Request.CreateResponse(HttpStatusCode.OK, Responsemodel);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}