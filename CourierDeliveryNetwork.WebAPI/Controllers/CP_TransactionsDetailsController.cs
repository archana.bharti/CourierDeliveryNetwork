﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.TransactionDetailsModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Transactions")]
    public class CP_TransactionsDetailsController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

      
        [HttpPost]
        [Route("transactionList")]
        [SecureResource]
        public HttpResponseMessage GetTransactionList([FromBody]TransactionRequestModel model)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 carrierProviderId = DbContext.Database.SqlQuery<Int64>("Select carrierProviderId from UserToken where Token={0}", token).FirstOrDefault();

                Int32 TotalOrders = DbContext.Database.SqlQuery<Int32>("Select Count(*) from OrderFinal ORF INNER JOIN OrderTransactions OT on ORF.OrderID=OT.OrderID where carrierProviderId={0}", carrierProviderId).FirstOrDefault();

                var RequestedOrders = DbContext.Database.SqlQuery<Int64>("GetAllOrders @Info=@Info,@carrierProviderId=@carrierProviderId,@datefrom=@datefrom,@dateto=@dateto,@offset=@offset,@limit=@limit",
                                               new SqlParameter("Info", "Transaction"),
                                               new SqlParameter("carrierProviderId", carrierProviderId),
                                               new SqlParameter("datefrom", model.requestdate.from),
                                               new SqlParameter("dateto", model.requestdate.to),
                                               new SqlParameter("offset", model.offset),
                                               new SqlParameter("limit", model.limit)
                                               ).ToList();

                TransactionResponse Responsemodel = new TransactionResponse();

                foreach (var orderid in RequestedOrders)
                {
                    Transaction obj = new Transaction();

                    obj.customer= DbContext.Database.SqlQuery<Customer>("GetTransactionDetail @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                               new SqlParameter("Orderid", orderid),
                                               new SqlParameter("TypeofInfo", "customer")
                                               ).FirstOrDefault();

                    obj.pickUp = DbContext.Database.SqlQuery<PickUp>("GetTransactionDetail @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("Orderid", orderid),
                                              new SqlParameter("TypeofInfo", "pickUp")
                                              ).FirstOrDefault();

                    obj.dropOff = DbContext.Database.SqlQuery<DropOff>("GetTransactionDetail @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                             new SqlParameter("Orderid", orderid),
                                             new SqlParameter("TypeofInfo", "dropOff")
                                             ).FirstOrDefault();

                    obj.orderDetails = DbContext.Database.SqlQuery<OrderDetails>("GetTransactionDetail @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                             new SqlParameter("Orderid", orderid),
                                             new SqlParameter("TypeofInfo", "orderDetails")
                                             ).FirstOrDefault();

                    obj.paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("GetTransactionDetail @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                            new SqlParameter("Orderid", orderid),
                                            new SqlParameter("TypeofInfo", "paymentDetails")
                                            ).FirstOrDefault();

                    Responsemodel.data.Add(obj);
                }
                if (Responsemodel != null)
                {
                    Responsemodel.code = Convert.ToInt32(CommonModel.SuccessCode);
                    Responsemodel.offset = Convert.ToInt32(model.offset);
                    Responsemodel.limit = Convert.ToInt32(model.limit);
                    Responsemodel.total = Convert.ToInt32(TotalOrders);
                }
                else
                {
                    Responsemodel.code = Convert.ToInt32(CommonModel.BadRequest);
                }
                var message = Request.CreateResponse(HttpStatusCode.OK, Responsemodel);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}