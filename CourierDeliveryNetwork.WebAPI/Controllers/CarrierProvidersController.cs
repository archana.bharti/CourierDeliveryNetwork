﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/CarrierProviders")]
    public class CarrierProvidersController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        
        [HttpPost]
        [Route("Registration")]
        public HttpResponseMessage RegisterCarrierProviders([FromBody]CarrierProviderModel model)
        {
            CarrierProviderModelResponse objResponse = new CarrierProviderModelResponse();       
            try
            {
                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string token = Token.GetUniqueToken();
                    int value = DbContext.Database.ExecuteSqlCommand("RegisterCarrierProvider @Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Password=@Password,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@Address=@Address,@DOB=@DOB,@Gender=@Gender,@CompanyName=@CompanyName,@CompanyRegistrationNo=@CompanyRegistrationNo,@CompanyLogo=@CompanyLogo,@TIN_VAT=@TIN_VAT,@Description=@Description,@CarrierProviderToken=@CarrierProviderToken",
                                    new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                                    new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                                    new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                                    new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                                    new SqlParameter("Password", (object)model.Password ?? DBNull.Value),
                                    new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                                    new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                                    new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                                    new SqlParameter("Address", (object)model.Address ?? DBNull.Value),
                                    new SqlParameter("DOB", (object)model.DOB ?? DBNull.Value),
                                    new SqlParameter("Gender", (object)model.Gender ?? DBNull.Value),
                                    
                                    new SqlParameter("CompanyName", (object)model.company.CompanyName ?? DBNull.Value),
                                    new SqlParameter("CompanyRegistrationNo", (object)model.company.CompanyRegistrationNo ?? DBNull.Value),
                                    new SqlParameter("CompanyLogo", (object)model.company.CompanyLogo ?? DBNull.Value),
                                    new SqlParameter("TIN_VAT", (object)model.company.TIN_VAT ?? DBNull.Value),
                                                                                                         
                                    new SqlParameter("Description", (object)model.Description ?? DBNull.Value),
                                    new SqlParameter("CarrierProviderToken", (object)token ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objResponse.message = "Registered Successfully";
                        objResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage CarrierProviderLogin(CarrierProviderLoginModel model)
        {
            LoginMainResponse objResponse = new LoginMainResponse();
            try
            {
                objResponse.response.data = DbContext.Database.SqlQuery<LoginResponseData>("ValidateCarrierProvider @Email=@Email,@password=@password",
                        new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                        new SqlParameter("password", (object)model.Password ?? DBNull.Value)).FirstOrDefault();
                objResponse.response.token = objResponse.response.data.Token;
                if (objResponse.response != null)
                {
                    objResponse.response.message = "Login Successfully";
                    objResponse.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objResponse.response.message = "Email Or password does not match";
                    objResponse.response.code = CommonModel.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, objResponse);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objResponse);

        }

       
        [HttpPost]
        [Route("refreshToken")]
        public HttpResponseMessage refreshToken()
        {
            RefreshTokenObject objResponse = new RefreshTokenObject();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 CarrierProviderID = DbContext.Database.SqlQuery<Int64>("select CarrierProviderID from UserToken where Token={0}", token).FirstOrDefault();
                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string Newtoken = Token.GetUniqueToken();
                    int value = DbContext.Database.ExecuteSqlCommand("GenerateNewToken @ID=@ID,@TokenFor=@TokenFor,@Newtoken=@Newtoken",
                                    new SqlParameter("@ID", CarrierProviderID),
                                    new SqlParameter("TokenFor", "CARPROVIDER"),
                                    new SqlParameter("Newtoken", Newtoken)
                                   );
                    if (value > 0)
                    {
                        objResponse.message = "Token Refreshed Successfully!!";
                        objResponse.code = CommonModel.SuccessCode;
                        objResponse.userToken = Newtoken;
                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

       
        [HttpPost]
        [Route("forgotPassword")]
        public HttpResponseMessage ProviderForgetPassword(CarrierProviderForgetPwd model)
        {
            try
            {
                CarrierProviderForgetPasswordObject objRes = new CarrierProviderForgetPasswordObject();
                int value = DbContext.Database.ExecuteSqlCommand("CarrierProviderForgetPassword @Email=@Email,@Password=@Password",
                      new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                      new SqlParameter("Password", (object)model.Password ?? DBNull.Value));

                if (value > 0)
                {
                    objRes.response.message = "Password reset link sent Successfully";
                    objRes.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objRes.response.code = CommonModel.BadRequest;
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objRes);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        
        [HttpPost]
        [Route("accountDetails")]
        [SecureResource]
        public HttpResponseMessage CarrierProviderDetail()
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 Carrierproviderid = DbContext.Database.SqlQuery<Int64>("Select Carrierproviderid from UserToken where Token={0}", token).FirstOrDefault();

                CarProviderResponseObject model = new CarProviderResponseObject();

                model.response.carProviderDetails = DbContext.Database.SqlQuery<CarrierProviderModel>("GetCarrierProviderDetail @Carrierproviderid=@Carrierproviderid,@TypeofInfo=@TypeofInfo",
                                               new SqlParameter("Carrierproviderid", Carrierproviderid),
                                               new SqlParameter("TypeofInfo", "carProviderDetails")
                                               ).FirstOrDefault();

                model.response.carProviderDetails.company = DbContext.Database.SqlQuery<Company>("GetCarrierProviderDetail @Carrierproviderid=@Carrierproviderid,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("Carrierproviderid", Carrierproviderid),
                                              new SqlParameter("TypeofInfo", "companyDetails")
                                              ).FirstOrDefault();


                if (model != null)
                {
                    model.response.code = Convert.ToInt32(CommonModel.SuccessCode);

                }
                else
                {
                    model.response.code = Convert.ToInt32(CommonModel.BadRequest);

                }

                var message = Request.CreateResponse(HttpStatusCode.OK, model);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        
        [HttpPost]
        [Route("updateAccountDetails")]
        [SecureResource]
        public HttpResponseMessage EditAccountDetails(CarrierProviderModel model)
        {
            try
            {
                CarProviderResponseObject objRes = new CarProviderResponseObject();
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 Carrierproviderid = DbContext.Database.SqlQuery<Int64>("Select Carrierproviderid from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("EditCarrierProviderDetails @CarrierProviderID=@CarrierProviderID,@Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Gender=@Gender,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@Address=@Address,@DOB=@DOB",
                         new SqlParameter("CarrierProviderID", Carrierproviderid),
                         new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                         new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                         new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                         new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                         new SqlParameter("Gender", (object)model.Gender ?? DBNull.Value),
                         new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                         new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                         new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                         new SqlParameter("Address", (object)model.Address ?? DBNull.Value),
                         new SqlParameter("DOB", (object)model.DOB ?? DBNull.Value)

                         );

                if (value > 0)
                {
                    objRes.response.code = Convert.ToInt32(CommonModel.SuccessCode);
                }
                else
                {
                    objRes.response.code = Convert.ToInt32(CommonModel.BadRequest);
                }

                objRes.response.message = "Record Updated Successfully!!";

                objRes.response.carProviderDetails = DbContext.Database.SqlQuery<CarrierProviderModel>("GetCarrierProviderDetail @Carrierproviderid=@Carrierproviderid,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("Carrierproviderid", Carrierproviderid),
                                              new SqlParameter("TypeofInfo", "carProviderDetails")
                                              ).FirstOrDefault();

                objRes.response.carProviderDetails.company = DbContext.Database.SqlQuery<Company>("GetCarrierProviderDetail @Carrierproviderid=@Carrierproviderid,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("Carrierproviderid", Carrierproviderid),
                                              new SqlParameter("TypeofInfo", "companyDetails")
                                              ).FirstOrDefault();

                return Request.CreateResponse(HttpStatusCode.OK, objRes);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            
        }
    }
}