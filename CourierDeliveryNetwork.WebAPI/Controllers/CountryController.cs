﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Models;


namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Country")]
    public class CountryController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        
       
        [HttpPost]
        [Route("Countries")]
        public IHttpActionResult GetCountryList()
        {
            CountryResponse objCountryResponse = new CountryResponse();
            //List<CountryModel> objtblCountry = null;
            try
            {
               
                objCountryResponse.response.countries = DbContext.Database.SqlQuery<CountryModel>("GetCountryList").ToList();
                if(objCountryResponse.response.countries.Count >0)
                {
                    objCountryResponse.response.code = CommonModel.SuccessCode;
                
                }
                else if(objCountryResponse.response.countries.Count == 0)
                {
                    objCountryResponse.response.code = CommonModel.NoRecord;
                }
               
               
            }
            catch (Exception ex)
            {

                objCountryResponse.response.code = CommonModel.FailureCode;
                objCountryResponse.response.code = CommonModel.Description;

            }

            return Json(objCountryResponse);
        }

       


    }
}
