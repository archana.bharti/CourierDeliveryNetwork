﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.AddCommentModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Comment")]
    public class CustomerAddCommentController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpPost]
        [Route("addComment")]
        [SecureResource]
        public HttpResponseMessage CustomerAddComment(AddCommentReq req)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                AddCommentResponse objAddCommentResponse = new AddCommentResponse();

                int value = DbContext.Database.ExecuteSqlCommand("AddComment @CustomerId=@CustomerId,@RequirementID=@RequirementID,@Comment=@Comment",
                            new SqlParameter("CustomerId", UserId),
                            new SqlParameter("RequirementID", req.RequirementID),
                            new SqlParameter("Comment", req.Comment));
            

                objAddCommentResponse.comment = DbContext.Database.SqlQuery<AddComment>("AddCommentResponse @CustomerId=@CustomerId",
                                                new SqlParameter("CustomerId", UserId)).ToList();

                if(objAddCommentResponse.comment.Count>0)
                {
                    objAddCommentResponse.code = CommonModel.SuccessCode;
                }
                else
                {
                    objAddCommentResponse.code = CommonModel.BadRequest;
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objAddCommentResponse);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
          
        }




        [HttpPost]
        [Route("CommentsList")]
        [SecureResource]
        public HttpResponseMessage CommentsList(CommentListRequestModel objCommentListRequestModel)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                //var value = DbContext.Database.SqlQuery<Comment>("GetallCommentList @RequirmentId=@RequirmentId",
                //                                       new SqlParameter("RequirmentId", objCommentListRequestModel.RequirmentId)).ToList();
                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();
                List<Comment> value = DbContext.Database.SqlQuery<Comment>("GetCommentList @offset=@offset,@limit=@limit,@RequirementID=@RequirementID,@searchKey=@searchKey,@CustomerID=@CustomerID",
                                                           new SqlParameter("offset", objCommentListRequestModel.offset),
                                                           new SqlParameter("limit", objCommentListRequestModel.limit),
                                                           new SqlParameter("RequirementID", objCommentListRequestModel.RequirmentId),
                                                           new SqlParameter("searchKey", objCommentListRequestModel.searchKey),
                                                           new SqlParameter("CustomerID", UserId)).ToList();



               
                CommentListResponseModel objCommentListResponseModel = new CommentListResponseModel();
                foreach(var item in value)
                {

                    CommentListData obj = new CommentListData();
                    obj.comment = item;
                    obj.userDetails= DbContext.Database.SqlQuery<CommentListResponseUserDetails>("GetCustomerDetailById @CustomerID=@CustomerID",
                                                                                    new SqlParameter("CustomerID", UserId)).FirstOrDefault();

                    objCommentListResponseModel.response.data.Add(obj);
                }
                objCommentListResponseModel.response.code =  CommonModel.SuccessCode;
                objCommentListResponseModel.response.limit =Convert.ToInt32(objCommentListRequestModel.limit);
                objCommentListResponseModel.response.offset =Convert.ToInt32(objCommentListRequestModel.offset);
                objCommentListResponseModel.response.total =Convert.ToInt32(objCommentListRequestModel.offset) + Convert.ToInt32(objCommentListRequestModel.limit);

                var message = Request.CreateResponse(HttpStatusCode.Created, objCommentListResponseModel);
                return message;
            }
               catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
