﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.CartListModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Cart")]
    public class CustomerCartListController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpPost]
        [Route("CartList")]
        [SecureResource]
        public HttpResponseMessage CustomerCartList()
        {
            try
            {

                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();
                CartListMainModelObject model = new CartListMainModelObject();

                model.response.cartList = DbContext.Database.SqlQuery<CartListMainModel>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                   new SqlParameter("CustomerId", UserId),
                                                    new SqlParameter("TypeofInfo", "providerDetails")).ToList(); 
                for(int i=0; i< model.response.cartList.Count;i++)
                {
                    //for provider details
                    model.response.cartList[i].providerDetails = DbContext.Database.SqlQuery<ProviderDetails>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                   new SqlParameter("CustomerId", UserId),
                                                    new SqlParameter("TypeofInfo", "providerDetails")).FirstOrDefault();
                    //for currency
                    model.response.cartList[i].orderDetails.currency = DbContext.Database.SqlQuery<Currency>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "currency")).FirstOrDefault();


                    //for order details
                    model.response.cartList[i].orderDetails = DbContext.Database.SqlQuery<OrderDetails>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "orderDetails")).FirstOrDefault();

                    //for PaymentDetails
                    model.response.cartList[i].paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "paymentDetails")).FirstOrDefault();

                    //for PickUp
                    model.response.cartList[i].pickUp = DbContext.Database.SqlQuery<PickUp>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "pickUp")).FirstOrDefault();

                    //for dropOff
                    model.response.cartList[i].dropOff = DbContext.Database.SqlQuery<DropOff>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "dropOff")).FirstOrDefault();

                    //for Dimensions
                    model.response.cartList[i].parcelDetails.dimensions = DbContext.Database.SqlQuery<Dimensions>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "dimensions")).FirstOrDefault();

                    //for ParcelDetails
                    model.response.cartList[i].parcelDetails = DbContext.Database.SqlQuery<ParcelDetails>("GetCustomerCartList @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("CustomerId", UserId),
                                                   new SqlParameter("TypeofInfo", "parcelDetails")).FirstOrDefault();

                }

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
           
        }

    }
}
