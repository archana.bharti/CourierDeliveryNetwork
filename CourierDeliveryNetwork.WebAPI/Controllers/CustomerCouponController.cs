﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.CouponModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Coupon")]
    public class CustomerCouponController : ApiController
    {
        
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpGet]
        [Route("ApplyCouponCode")]
        [SecureResource]
        public HttpResponseMessage CutomerApplyCouponCode(string CouponCode)
        {
            try
            {

                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                //Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();
                CustomerCouponModelObject objResp = new CustomerCouponModelObject();

                objResp.response.coupon = DbContext.Database.SqlQuery<CustomerCouponModel>("ValidateCouponCode @CouponCode=@CouponCode",
                                            new SqlParameter("CouponCode", CouponCode)).FirstOrDefault();
                
                if(objResp.response.coupon!=null)
                {
                    objResp.response.code = CommonModel.SuccessCode;
                    objResp.response.message = "Coupon Code is applied successfully";
                }
                else
                {
                    objResp.response.code = CommonModel.SuccessCode;
                    objResp.response.message = "Coupon Code is Expired";
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objResp);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
