﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.CustomerBookOrderModel;
using static CourierDeliveryNetwork.WebAPI.Models.CustomerBookOrderModel.CustomerBookOrderModelRequest;
using static CourierDeliveryNetwork.WebAPI.Models.OrderHistory;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Order")]


    public class CustomerOrderController : ApiController
    {

        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpPost]
        [Route("OrderHistory")]
        [SecureResource]
        public HttpResponseMessage CustomerOrderHistory(CustomerOrderHistoryRequest objCustomerOrderHistoryRequest)
        {
           // OrderHistoryRequestResponse objOrderHistoryRequestResponse = new OrderHistoryRequestResponse();
            try
            {
                
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                
                ResponseMainModel model1 = new ResponseMainModel();


                model1.response.order_history = DbContext.Database.SqlQuery<CustomerOrderHistory>("GetCustomerOrderFinal @CustomerId=@CustomerId,@Offset=@Offset,@Limit=@Limit",
                                                                       new SqlParameter("CustomerId", UserId),
                                                                      new SqlParameter("Offset", objCustomerOrderHistoryRequest.Offset),
                                                                          new SqlParameter("Limit", objCustomerOrderHistoryRequest.Limit)).ToList();


               
                //model1.response.offset = objCustomerOrderHistoryRequest.Offset;
                //model1.response.limit = objCustomerOrderHistoryRequest.Limit;
                // model1.response.total= objCustomerOrderHistoryRequest.Offset+ objCustomerOrderHistoryRequest.Limit;
                for (int i=0; i<model1.response.order_history.Count; i++)
                {
                    
                    model1.response.order_history[i].dropOff = DbContext.Database.SqlQuery<DropOff>("GetCustomerOrderHistory @CustomerId=@CustomerId,@Offset=@Offset,@Limit=@Limit,@SearchText=@SearchText",
                                                                          new SqlParameter("CustomerId", UserId),
                                                                          new SqlParameter("Offset", objCustomerOrderHistoryRequest.Offset),
                                                                          new SqlParameter("Limit", objCustomerOrderHistoryRequest.Limit),
                                                                          new SqlParameter("SearchText", objCustomerOrderHistoryRequest.SearchKey)).FirstOrDefault();

                    model1.response.order_history[i].pickUp = DbContext.Database.SqlQuery<PickUp>("GetCustomerOrderHistory @CustomerId=@CustomerId,@Offset=@Offset,@Limit=@Limit,@SearchText=@SearchText",
                                                                          new SqlParameter("CustomerId", UserId),
                                                                          new SqlParameter("Offset", objCustomerOrderHistoryRequest.Offset),
                                                                          new SqlParameter("Limit", objCustomerOrderHistoryRequest.Limit),
                                                                          new SqlParameter("SearchText", objCustomerOrderHistoryRequest.SearchKey)).FirstOrDefault();

                    //model1.response.order_history[i].currency = DbContext.Database.SqlQuery<Currency>("GetCustomerOrderHistory @CustomerId=@CustomerId,@Offset=@Offset,@Limit=@Limit,@SearchText=@SearchText",
                    //                                                      new SqlParameter("CustomerId", UserId),
                    //                                                      new SqlParameter("Offset", objCustomerOrderHistoryRequest.Offset),
                    //                                                      new SqlParameter("Limit", objCustomerOrderHistoryRequest.Limit),
                    //                                                      new SqlParameter("SearchText", objCustomerOrderHistoryRequest.SearchKey)).FirstOrDefault();
                }
            


                if (model1.response.order_history.Count>0)
                {
                    model1.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    model1.response.code = CommonModel.BadRequest;
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, model1);
                return message;
            }
             catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost]
        [Route("BookOrder")]
        [SecureResource]
        public HttpResponseMessage CustomerBookHistory(BookOrderModelResponse modelabc)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();
                OrderBookResponse objOrderBookResponse = new OrderBookResponse();

             int value =  DbContext.Database.ExecuteSqlCommand("BookOrder @CustomerId=@CustomerId,@Height=@Height,@Width=@Width,@Length=@Length,@Weight=@Weight,@P_StreetAddress=@P_StreetAddress,@P_Landmark=@P_Landmark,@P_Country=@P_Country,@P_State=@P_State,@P_Pincode=@P_Pincode,@D_StreetAddress=@D_StreetAddress,@D_Landmark=@D_Landmark,@D_Country=@D_Country,@D_State=@D_State,@D_Pincode=@D_Pincode,@DeliveryType=@DeliveryType,@DocumentType=@DocumentType,@ItemValue=@ItemValue,@Description=@Description,@ServiceTax=@ServiceTax,@VAT=@VAT,@Discount=@Discount,@TotalPayable=@TotalPayable,@FirstName=@FirstName,@LastName=@LastName,@Gender=@Gender,@Email=@Email,@Mobile=@Mobile,@CountryID=@CountryID,@StateID=@StateID,@CityID=@CityID,@PD_Pincode=@PD_Pincode,@Current_Address=@Current_Address",
                                                  new SqlParameter("CustomerId", UserId),
                                                  //new SqlParameter("TypeofInfo", "dimensions"),
                                                  new SqlParameter("Height", modelabc.parcelDetails.dimensions.Height),
                                                  new SqlParameter("Width", modelabc.parcelDetails.dimensions.Width),
                                                  new SqlParameter("Length", modelabc.parcelDetails.dimensions.Length),
                                                  new SqlParameter("Weight", modelabc.parcelDetails.dimensions.Weight),
                                                  new SqlParameter("P_StreetAddress", (object)modelabc.parcelDetails.pickUp.StreetAddress ?? DBNull.Value),
                                                  new SqlParameter("P_Landmark", (object)modelabc.parcelDetails.pickUp.Landmark ?? DBNull.Value),
                                                  new SqlParameter("P_Country", (object)modelabc.parcelDetails.pickUp.Country ?? DBNull.Value),
                                                  new SqlParameter("P_State", (object)modelabc.parcelDetails.pickUp.State ?? DBNull.Value),
                                                  new SqlParameter("P_Pincode", (object)modelabc.parcelDetails.pickUp.Pincode ?? DBNull.Value),
                                                  new SqlParameter("D_StreetAddress", modelabc.parcelDetails.dropOff.StreetAddress),
                                                  new SqlParameter("D_Landmark", modelabc.parcelDetails.dropOff.Landmark),
                                                  new SqlParameter("D_Country", modelabc.parcelDetails.dropOff.Country),
                                                  new SqlParameter("D_State", modelabc.parcelDetails.dropOff.State),
                                                  new SqlParameter("D_Pincode", modelabc.parcelDetails.dropOff.Pincode),
                                                  new SqlParameter("DeliveryType", modelabc.parcelDetails.DeliveryType),
                                                  new SqlParameter("DocumentType", modelabc.parcelDetails.DocumentType),
                                                  new SqlParameter("ItemValue", modelabc.parcelDetails.ItemValue),
                                                  new SqlParameter("Description", modelabc.parcelDetails.Description),
                                                  new SqlParameter("ServiceTax", modelabc.Amount.ServiceTax),
                                                  new SqlParameter("VAT", modelabc.Amount.VAT),
                                                  new SqlParameter("Discount", modelabc.Amount.Discount),
                                                  new SqlParameter("TotalPayable", modelabc.Amount.TotalPayable),
                                                  new SqlParameter("FirstName", modelabc.personalDetails.FirstName),
                                                  new SqlParameter("LastName", modelabc.personalDetails.LastName),
                                                  new SqlParameter("Gender", modelabc.personalDetails.Gender),
                                                  new SqlParameter("Email", modelabc.personalDetails.Email),
                                                  new SqlParameter("Mobile", modelabc.personalDetails.Mobile),
                                                  new SqlParameter("CountryID", modelabc.personalDetails.CountryID),
                                                  new SqlParameter("StateID", modelabc.personalDetails.StateID),
                                                  new SqlParameter("CityID", modelabc.personalDetails.CityID),
                                                  new SqlParameter("PD_Pincode", modelabc.personalDetails.Pincode),
                                                  new SqlParameter("Current_Address", modelabc.personalDetails.Current_Address)
                                               );

                if(value >0)
                {
                    objOrderBookResponse.code = CommonModel.SuccessCode;

                }
                else
                {
                    objOrderBookResponse.code = CommonModel.BadRequest;
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objOrderBookResponse);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
