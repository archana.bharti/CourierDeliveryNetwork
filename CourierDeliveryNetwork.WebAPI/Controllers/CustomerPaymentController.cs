﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CourierDeliveryNetwork.WebAPI.Models;

using static CourierDeliveryNetwork.WebAPI.Models.CustomerPaymentModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Payment")]
    public class CustomerPaymentController : ApiController
    {

        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpPost]
        [Route("makePayment")]
        [SecureResource]
        public HttpResponseMessage makePayment(CustomerMakePaymentRequest model)
        {
            try
            {
                
                CustomerPaymentDetailObject ResModel = new CustomerPaymentDetailObject();

                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                Int32 TotalOrders = DbContext.Database.SqlQuery<Int32>("Select Count(*) from OrderParcelDetails OPF where CustomerId={0}", UserId).FirstOrDefault();



                int value = DbContext.Database.ExecuteSqlCommand("GetTransactionInfo @TransactionID=@TransactionID,@OrderID=@OrderID,@PaymentType=@PaymentType,@CurrencyCode=@CurrencyCode",
                             
                             new SqlParameter("TransactionID", model.TransactionID),
                             new SqlParameter("OrderID", model.OrderID),
                             new SqlParameter("PaymentType", model.PaymentType),
                             new SqlParameter("CurrencyCode", model.CurrencyCode));


                ResModel.response.paymentData = DbContext.Database.SqlQuery<CustomerPaymentDetailModel>("GetAllPaymentInfo @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                                        new SqlParameter("CustomerId", UserId),
                                                                        new SqlParameter("TypeofInfo", "paymentData")).FirstOrDefault();

                var RequestedOrders = DbContext.Database.SqlQuery<Int64>("GetAllPaymentInfo @CustomerId=@CustomerId,@TypeofInfo=@TypeofInfo",
                                                                        new SqlParameter("CustomerId", UserId),
                                                                        new SqlParameter("TypeofInfo", "ordersList")).ToList();



                foreach (var orderid in RequestedOrders)
                {
                    OrdersList obj1 = new OrdersList();


                    //for currency
                    obj1.orderDetails.currency = DbContext.Database.SqlQuery<Currency>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "currency")
                                                    ).FirstOrDefault();
                
                    //for order details
                    obj1.orderDetails = DbContext.Database.SqlQuery<OrderDetails>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "orderDetails")
                                                    ).FirstOrDefault();


                    //for PaymentDetails
                    obj1.paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "paymentDetails")
                                                    ).FirstOrDefault();

                    //for PickUp
                    obj1.pickUp = DbContext.Database.SqlQuery<PickUp>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "pickUp")
                                                    ).FirstOrDefault();

                    //for dropOff
                    obj1.dropOff = DbContext.Database.SqlQuery<DropOff>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "dropOff")
                                                    ).FirstOrDefault();

                    //for Dimession
                    obj1.parcelDetails.dimensions = DbContext.Database.SqlQuery<Dimensions>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "dimensions")
                                                    ).FirstOrDefault();

                    //for ParcelDetails
                    obj1.parcelDetails = DbContext.Database.SqlQuery<ParcelDetails>("MakePayment @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "parcelDetails")
                                                    ).FirstOrDefault();


                    ResModel.response.paymentData.ordersList.Add(obj1);
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, ResModel);
                return message;
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
