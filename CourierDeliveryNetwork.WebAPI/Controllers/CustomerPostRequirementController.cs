﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.CustomerPostRequirementModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Requirement")]
    public class CustomerPostRequirementController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        public List<string> thumbURL = new List<string>();
        public List<string> imagesURL = new List<string>();

        [SecureResource]
        [HttpPost]
        [Route("PostRequirement")]
        public HttpResponseMessage PostRequirement(PostReqirementModel objPostRequrimentObject)
        {
            try
            {
                PostRequrimentResponse objPostRequrimentResponse = new PostRequrimentResponse();

                foreach (var item in objPostRequrimentObject.thumbURL)
                {
                    byte[] thumbURLbytes = Convert.FromBase64String(item);

                    using (MemoryStream mStream = new MemoryStream(thumbURLbytes))
                    {
                        Image postedImg = Image.FromStream(mStream);
                        var filePath = "~/Images/CourierImages" + 1 + ".jpg";
                        postedImg.Save(HostingEnvironment.MapPath("~" + filePath), ImageFormat.Png);// postedImg.RawFormat);
                        thumbURL.Add(filePath);
                    }

                }

                foreach (var items in objPostRequrimentObject.imagesURL)
                {
                    byte[] imagesURLbytes = Convert.FromBase64String(items);

                    using (MemoryStream mStream = new MemoryStream(imagesURLbytes))
                    {
                        Image postedImg = Image.FromStream(mStream);
                        var filePath = "~/Images/CourierImages/" + 1 + ".jpg";
                        postedImg.Save(HostingEnvironment.MapPath("~" + filePath), ImageFormat.Png);// postedImg.RawFormat);
                        imagesURL.Add(filePath);
                    }
                }

                objPostRequrimentObject.imagesURL = imagesURL;
                objPostRequrimentObject.thumbURL = thumbURL;

                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();


                {

                    objPostRequrimentResponse.postReq_List = DbContext.Database.SqlQuery<PostReqirementModel>("AddPost @CustomerID=@CustomerID,@RequirementID=@RequirementID,@Category=@Category,@Title=@Title,@MinimumAmount=@MinimumAmount,@ValidTill=@ValidTill,@Description=@Description,@Status=@Status,@CurrencyCode=@CurrencyCode",
                         new SqlParameter("CustomerID", UserId),
                        new SqlParameter("RequirementID", objPostRequrimentObject.RequirementID),

                                   new SqlParameter("Category", objPostRequrimentObject.Category),
                                   new SqlParameter("Title", objPostRequrimentObject.Title),
                                   new SqlParameter("MinimumAmount", objPostRequrimentObject.MinimumAmount),
                                   new SqlParameter("ValidTill", objPostRequrimentObject.ValidTill),
                                   new SqlParameter("Description", objPostRequrimentObject.Description),
                                   new SqlParameter("Status", objPostRequrimentObject.Status),
                                   new SqlParameter("CurrencyCode", objPostRequrimentObject.CurrencyCode)).FirstOrDefault();




                    foreach (var iten in objPostRequrimentObject.imagesURL)
                    {
                        if (objPostRequrimentResponse.postReq_List!=null)
                        {
                            int value1 = DbContext.Database.ExecuteSqlCommand("SaveImages @RequirementID=@RequirementID,@ImageUrl=@ImageUrl,@ThumbUrl=@ThumbUrl",
                                       new SqlParameter("RequirementID", objPostRequrimentResponse.postReq_List.RequirementID),
                                       new SqlParameter("ImageUrl", objPostRequrimentObject.imagesURL),
                                       new SqlParameter("ThumbUrl", objPostRequrimentObject.thumbURL));
                        }
                    }
                }

                objPostRequrimentResponse.code = CommonModel.SuccessCode;

                var message = Request.CreateResponse(HttpStatusCode.Created, objPostRequrimentResponse);
                return message;
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        

        [SecureResource]
        [HttpPost]
        [Route("RequirementList")]
        public HttpResponseMessage RequirementList(RequireListRequest objRequireListRequest)
        {
            try
            {

                RequirmentListResponse objRequirmentListResponse = new RequirmentListResponse();
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                objRequirmentListResponse.response.data = DbContext.Database.SqlQuery<ResponseData>("GetRequirmentList @CustomerId=@CustomerId,@offset=@offset,@limit=@limit,@searchKey=@searchKey,@catId=@catId",
                                                      new SqlParameter("CustomerId", UserId),
                                                      new SqlParameter("offset", objRequireListRequest.offset),
                                                      new SqlParameter("limit", objRequireListRequest.limit),
                                                      new SqlParameter("searchKey", objRequireListRequest.searchKey),
                                                      new SqlParameter("catId", objRequireListRequest.catId)).ToList();

                foreach (var item in objRequirmentListResponse.response.data)
                {

                    item.imagesURL = DbContext.Database.SqlQuery<string>("GetImagesAgainstBid @RequirementID=@RequirementID,@Type=@Type",
                                                          new SqlParameter("RequirementID", item.RequirementID),
                                                          new SqlParameter("Type", "Image")).ToList();

                    item.thumbURL = DbContext.Database.SqlQuery<string>("GetImagesAgainstBid @RequirementID=@RequirementID,@Type=@Type",
                                                          new SqlParameter("RequirementID", item.RequirementID),
                                                            new SqlParameter("Type", "Thumb")).ToList();


                }
                objRequirmentListResponse.response.limit = objRequireListRequest.limit;
                objRequirmentListResponse.response.offset = objRequireListRequest.offset;
                objRequirmentListResponse.response.total = objRequireListRequest.limit + objRequireListRequest.offset;
                objRequirmentListResponse.response.code = CommonModel.SuccessCode;

                var message = Request.CreateResponse(HttpStatusCode.Created, objRequirmentListResponse);
                return message;
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [SecureResource]
        [HttpGet]
        [Route("RequirementDetail")]
        public HttpResponseMessage RequirementDetail(Int32 RequirementID)
        {
            try
            {
               RequirementDetailResponse objRequirementDetailResponse = new RequirementDetailResponse();
               objRequirementDetailResponse.data = DbContext.Database.SqlQuery<RequirmentResponseData>("GetRequirementDetails @RequirementID=@RequirementID",
                                                      new SqlParameter("RequirementID", RequirementID)
                                                     ).FirstOrDefault();

                if(objRequirementDetailResponse.data!=null)
                {
                    objRequirementDetailResponse.data.ImageUrl = DbContext.Database.SqlQuery<string>("GetImagesAgainstBid @RequirementID=@RequirementID,@Type=@Type",
                                                          new SqlParameter("RequirementID", RequirementID),
                                                          new SqlParameter("Type", "Image")).ToList();

                    objRequirementDetailResponse.data.ThumbUrl = DbContext.Database.SqlQuery<string>("GetImagesAgainstBid @RequirementID=@RequirementID,@Type=@Type",
                                                          new SqlParameter("RequirementID", RequirementID),
                                                            new SqlParameter("Type", "Thumb")).ToList();
                }


                objRequirementDetailResponse.code = CommonModel.SuccessCode;

                var message = Request.CreateResponse(HttpStatusCode.Created, objRequirementDetailResponse);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

              
        }


    }

}

