﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Customer")]
    public class CustomerRegistrationController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        
        [HttpPost]
        [Route("Register")]
        public HttpResponseMessage RegisterCustomer([FromBody]CustomerRegistrationModel model)
        {
            RegisterModelResponse objRegisterModelResponse = new RegisterModelResponse();
             //CustomerLoginModelResponse objCustomerLoginModelResponse = new CustomerLoginModelResponse();
            try
            {
                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string token = Token.GetUniqueToken();
                    int value =   DbContext.Database.ExecuteSqlCommand("RegisterCustomer @Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Password=@Password,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@SocialId=@SocialId,@IsActive=@IsActive,@DeviceID=@DeviceID,@UserToken=@UserToken,@StreetAddress=@StreetAddress,@LandMark=@LandMark",
                               
                                new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                                new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                                new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                                new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                                new SqlParameter("Password", (object)model.Password ?? DBNull.Value),
                                new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                                new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                                new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                                new SqlParameter("SocialId", (object)model.SocialId ?? DBNull.Value),
                                new SqlParameter("IsActive", (object)model.IsActive ?? DBNull.Value),
                                new SqlParameter("DOB", (object)model.DOB ?? DBNull.Value),
                                new SqlParameter("DeviceID", (object)model.DeviceID ?? DBNull.Value),
                                new SqlParameter("UserToken", (object)token ?? DBNull.Value),
                                new SqlParameter("StreetAddress", (object)model.StreetAddress ?? DBNull.Value),
                                new SqlParameter("LandMark", (object)model.LandMark ?? DBNull.Value)
                                
                                );

                    if(value>0)
                    {
                        objRegisterModelResponse.message = "Registered Successfully";
                        objRegisterModelResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objRegisterModelResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objRegisterModelResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
           
        }


       
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage CustomerLogin(CustomerLoginModel model)
        {
            CustomerLoginModelObject objCustomerLoginModelObject = new CustomerLoginModelObject();

            try
            {

                objCustomerLoginModelObject.response = DbContext.Database.SqlQuery<CustomerLoginModelResponse>("ValidateUser @Email=@Email,@password=@password",
                        new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                        new SqlParameter("password", (object)model.Password ?? DBNull.Value)).FirstOrDefault();

                if (objCustomerLoginModelObject.response!=null)
                    {
                        objCustomerLoginModelObject.response.message = "Login Successfully";
                        objCustomerLoginModelObject.response.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objCustomerLoginModelObject.response.message = "Email Or password does not match";
                        objCustomerLoginModelObject.response.code = CommonModel.BadRequest;
                    }
                    
                }

               
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, objCustomerLoginModelObject);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objCustomerLoginModelObject);

        }

        [HttpPost]
        [Route("AddressList")]
        
        [SecureResource]
        public HttpResponseMessage GetCustomerAddressList()
        {
            CustomerAddressListModelObject objCustomerAddressListModelObject = new CustomerAddressListModelObject();

            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                objCustomerAddressListModelObject.response.listdata = DbContext.Database.SqlQuery<CustomerAddressListModel>("GetAddressList").ToList();
                if(objCustomerAddressListModelObject.response.listdata.Count>0)
                {
                    objCustomerAddressListModelObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objCustomerAddressListModelObject.response.code = CommonModel.BadRequest;
                }
                
                var message = Request.CreateResponse(HttpStatusCode.Created, objCustomerAddressListModelObject);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("AddEditAddress")]
       
        [SecureResource]
        public HttpResponseMessage CustomerAddEditAddress(CustomerAddressListModel model)
        {
            CustomerAddressListModelObject objCustomerAddressListModelObject = new CustomerAddressListModelObject();
            CustomerAdd_EditAddressModelResponse objCustomerAdd_EditAddressModelResponse = new CustomerAdd_EditAddressModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("Add_EditAddress @FirstName=@FirstName,@LastName=@LastName,@PhoneNumber=@PhoneNumber,@StreetAddress=@StreetAddress,@Landmark=@Landmark,@Country=@Country,@State=@State,@City=@City,@Pincode=@Pincode,@Mode=@Mode",
                    new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                    new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                    new SqlParameter("PhoneNumber", (object)model.PhoneNumber ?? DBNull.Value),
                    new SqlParameter("StreetAddress", (object)model.StreetAddress ?? DBNull.Value),
                    new SqlParameter("Landmark", (object)model.Landmark ?? DBNull.Value),
                    new SqlParameter("Country", (object)model.Country ?? DBNull.Value),
                    new SqlParameter("State", (object)model.State ?? DBNull.Value),
                    new SqlParameter("City", (object)model.City ?? DBNull.Value),
                    new SqlParameter("Pincode", (object)model.Pincode ?? DBNull.Value),
                    new SqlParameter("Mode", (object)model.Mode ?? DBNull.Value)
                    );
                if(value>0)
                {
                   
                    objCustomerAddressListModelObject.response.code = CommonModel.SuccessCode;
                    model.Status = CommonModel.SuccessCode;
                }
                else
                {
                    objCustomerAddressListModelObject.response.code = CommonModel.BadRequest;
                }


                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost]
        [Route("MakeDefaultAddress")]
        [AcceptVerbs("POST")]
        [SecureResource]
        public HttpResponseMessage CustomerMakeDefaultAddress(CustomerMakeDefaultAddressModel model)
        {
            CustomerMakeDefaultAddressModelResponse objCustomerMakeDefaultAddressModelObject = new CustomerMakeDefaultAddressModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("SetDefaultAddress @CustomerID=@CustomerID,@AddressID=@AddressID",
                             new SqlParameter("CustomerID", (object)model.CustomerID ?? DBNull.Value),
                             new SqlParameter("AddressID", (object)model.AddressID ?? DBNull.Value));

                if(value>0)
                {
                    objCustomerMakeDefaultAddressModelObject.code = CommonModel.SuccessCode;
                }
                else
                {
                    objCustomerMakeDefaultAddressModelObject.code = CommonModel.BadRequest;
                }



                var message = Request.CreateResponse(HttpStatusCode.Created, objCustomerMakeDefaultAddressModelObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        
        [HttpPost]
        [Route("refreshToken")]
        public HttpResponseMessage refreshToken()
        {
            RefreshTokenObject objResponse = new RefreshTokenObject();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string Newtoken = Token.GetUniqueToken();

                    int value = DbContext.Database.ExecuteSqlCommand("GenerateNewToken @ID=@ID,@TokenFor=@TokenFor,@Newtoken=@Newtoken",
                                    new SqlParameter("@ID", UserId),
                                    new SqlParameter("TokenFor", "Customer"),
                                    new SqlParameter("Newtoken", Newtoken)
                                   );

                    if (value > 0)
                    {
                        objResponse.message = "Token Refreshed Successfully!!";
                        objResponse.code = CommonModel.SuccessCode;
                        objResponse.userToken = Newtoken;

                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }


        //for converting password in encypted form
        public string CalculateHash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }


    }
}
