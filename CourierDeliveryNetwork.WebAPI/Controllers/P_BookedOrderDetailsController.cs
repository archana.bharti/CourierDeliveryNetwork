﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.BookedOrderDetailsModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Providers")]
    public class P_BookedOrderDetailsController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        
        [HttpPost]
        [Route("orderDetails")]
        [SecureResource]
        public HttpResponseMessage GetOrderDetail([FromBody]OrderDetailsRequestModel model)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 providerid = DbContext.Database.SqlQuery<Int64>("Select providerid from UserToken where Token={0}", token).FirstOrDefault();

                MainOrderResponseObject Responsemodel = new MainOrderResponseObject();

                Responsemodel.response.customerDetails = DbContext.Database.SqlQuery<CustomerDetails>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                               new SqlParameter("providerid", providerid),
                                               new SqlParameter("OrderID", model.orderId),
                                               new SqlParameter("TypeofInfo", "customerDetails")
                                               ).FirstOrDefault();

                Responsemodel.response.pickUp = DbContext.Database.SqlQuery<PickUp>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("providerid", providerid),
                                                  new SqlParameter("OrderID", model.orderId),
                                               new SqlParameter("TypeofInfo", "pickUp")
                                               ).FirstOrDefault();

                Responsemodel.response.dropOff = DbContext.Database.SqlQuery<DropOff>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("providerid", providerid),
                                                  new SqlParameter("OrderID", model.orderId),
                                               new SqlParameter("TypeofInfo", "dropOff")
                                               ).FirstOrDefault();

                Responsemodel.response.packageDetails = DbContext.Database.SqlQuery<PackageDetails>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                                 new SqlParameter("providerid", providerid),
                                                 new SqlParameter("OrderID", model.orderId),
                                               new SqlParameter("TypeofInfo", "packageDetails")
                                               ).FirstOrDefault();

                Responsemodel.response.orderDetails = DbContext.Database.SqlQuery<OrderDetails>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("providerid", providerid),
                                              new SqlParameter("OrderID", model.orderId),
                                            new SqlParameter("TypeofInfo", "orderDetails")
                                            ).FirstOrDefault();

                Responsemodel.response.orderDetails.currency = DbContext.Database.SqlQuery<Currency>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                             new SqlParameter("providerid", providerid),
                                             new SqlParameter("OrderID", model.orderId),
                                           new SqlParameter("TypeofInfo", "currency")
                                           ).FirstOrDefault();

                Responsemodel.response.paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("GetProviderOrderDetail @providerid=@providerid,@OrderID=@OrderID,@TypeofInfo=@TypeofInfo",
                                              new SqlParameter("providerid", providerid),
                                              new SqlParameter("OrderID", model.orderId),
                                            new SqlParameter("TypeofInfo", "paymentDetails")
                                            ).FirstOrDefault();

                if (model != null)
                {
                    Responsemodel.response.code = Convert.ToInt32(CommonModel.SuccessCode);
                }
                else
                {
                    Responsemodel.response.code = Convert.ToInt32(CommonModel.BadRequest);
                }
                var message = Request.CreateResponse(HttpStatusCode.OK, Responsemodel);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}