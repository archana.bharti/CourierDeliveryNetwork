﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.ProviderProfileModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Providers")]
    public class ProviderProfileController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

      
        [HttpPost]
        [Route("forgotPassword")]
        public HttpResponseMessage ProviderForgetPassword(ProviderForgetPassword model)
        {
            try
            {
                ProviderForgetPasswordObject objProviderForgetPasswordObject = new ProviderForgetPasswordObject();
                int value = DbContext.Database.ExecuteSqlCommand("ProviderForgetPassword @Email=@Email",
                      new SqlParameter("Email", (object)model.Email ?? DBNull.Value));

                if (value > 0)
                {
                    objProviderForgetPasswordObject.response.message = "Password reset link sent Successfully";
                    objProviderForgetPasswordObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objProviderForgetPasswordObject.response.code = CommonModel.BadRequest;
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objProviderForgetPasswordObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

       
        [HttpPost]
        [Route("ChangePassword")]
        [SecureResource]
        public HttpResponseMessage ProviderChangePassword(ProviderChangePasswordModel model)
        {
            ProviderChangePasswordModelResponse objProviderChangePasswordModelResponse = new ProviderChangePasswordModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select ProviderId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("ProviderChangePassword @Email=@Email,@Password=@Password",
                            new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                            new SqlParameter("Password", (object)model.Password ?? DBNull.Value));
                if (value > 0)
                {
                    objProviderChangePasswordModelResponse.code = CommonModel.SuccessCode;
                    objProviderChangePasswordModelResponse.message = "Password Changed";
                }
                else
                {
                    objProviderChangePasswordModelResponse.code = CommonModel.BadRequest;
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objProviderChangePasswordModelResponse);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        
        [HttpPost]
        [Route("accountDetails")]
        [SecureResource]
        public HttpResponseMessage ProviderDetailInfo()
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();


                Int64 providerid = DbContext.Database.SqlQuery<Int64>("Select providerid from UserToken where Token={0}", token).FirstOrDefault();

                ProviderResponseMainModel model = new ProviderResponseMainModel();

                model.response.userDetails = DbContext.Database.SqlQuery<UserDetails>("GetProviderDetail @providerid=@providerid,@TypeofInfo=@TypeofInfo",
                                               new SqlParameter("providerid", providerid),
                                               new SqlParameter("TypeofInfo", "userDetails")
                                               ).FirstOrDefault();

                model.response.bankDetails = DbContext.Database.SqlQuery<BankDetails>("GetProviderDetail @providerid=@providerid,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("providerid", providerid),
                                               new SqlParameter("TypeofInfo", "bankDetails")
                                               ).FirstOrDefault();

                model.response.companyDetails = DbContext.Database.SqlQuery<CompanyDetails>("GetProviderDetail @providerid=@providerid,@TypeofInfo=@TypeofInfo",
                                                  new SqlParameter("providerid", providerid),
                                               new SqlParameter("TypeofInfo", "companyDetails")
                                               ).FirstOrDefault();

                model.response.serviceDetails = DbContext.Database.SqlQuery<ServiceDetails>("GetProviderDetail @providerid=@providerid,@TypeofInfo=@TypeofInfo",
                                                 new SqlParameter("providerid", providerid),
                                               new SqlParameter("TypeofInfo", "serviceDetails")
                                               ).FirstOrDefault();

                if (model != null)
                {
                    model.response.code = Convert.ToInt32(CommonModel.SuccessCode);

                }
                else
                {
                    model.response.code = Convert.ToInt32(CommonModel.BadRequest);

                }

                var message = Request.CreateResponse(HttpStatusCode.OK, model);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

       
        [HttpPost]
        [Route("updateAccountDetails")]
        [SecureResource]
        public HttpResponseMessage EditProviderDetails(ProviderRegistrationModel model)
        {
            try
            {
                EditProviderDetailObject objEditProviderDetailObject = new EditProviderDetailObject();
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("EditProviderDetails @ProviderID=@ProviderID,@Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Gender=@Gender,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@Address=@Address,@DOB=@DOB",
                         new SqlParameter("ProviderID", ProviderID),
                         new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                         new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                         new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                         new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                          new SqlParameter("Gender", (object)model.Gender ?? DBNull.Value),
                         new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                         new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                         new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                         new SqlParameter("Address", (object)model.Address ?? DBNull.Value),
                         new SqlParameter("DOB", (object)model.DOB ?? DBNull.Value)

                         );

                if (value > 0)
                {
                    objEditProviderDetailObject.response.code = CommonModel.SuccessCode;
                    objEditProviderDetailObject.response.message = "Your Information is updated successfully";

                }
                else
                {
                    objEditProviderDetailObject.response.code = CommonModel.BadRequest;

                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objEditProviderDetailObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("updateCompanyDetails")]
        [SecureResource]
        public HttpResponseMessage EditProviderCompanyDetails(ProviderRegistrationModel model)
        {
            try
            {
                EditProviderCompanyDetailObject objEditProviderDetailObject = new EditProviderCompanyDetailObject();
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                ProviderRegistrationModel model1 = new ProviderRegistrationModel();


                int value = DbContext.Database.ExecuteSqlCommand("EditProviderCompanyDetails @CompanyRegistrationNo=@CompanyRegistrationNo,@CompanyName=@CompanyName,@Locality=@Locality,@CompanyLogo=@CompanyLogo,@Description=@Description",
                         new SqlParameter("@CompanyRegistrationNo", (object)model.CompanyRegistrationNo ?? DBNull.Value),
                         new SqlParameter("@CompanyName", (object)model.CompanyName ?? DBNull.Value),
                         new SqlParameter("@Locality", (object)model.Locality ?? DBNull.Value),
                         new SqlParameter("@CompanyLogo", (object)model.CompanyLogo ?? DBNull.Value),
                          new SqlParameter("@Description", (object)model.Description ?? DBNull.Value)
                         );

                if (value > 0)
                {
                    objEditProviderDetailObject.response.code = CommonModel.SuccessCode;
                    objEditProviderDetailObject.response.message = "Your Information is updated successfully";

                }
                else
                {
                    objEditProviderDetailObject.response.code = CommonModel.BadRequest;

                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objEditProviderDetailObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

       
        [HttpPost]
        [Route("providerCountriesList")]
        public IHttpActionResult OperatingCountries()
        {
            OperatingCountryResponse objOperatingCountryResponse = new OperatingCountryResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 providerid = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                objOperatingCountryResponse.opContresponse.countries = DbContext.Database.SqlQuery<OperatingCountryModel>("GetOperatingCountryList @providerid=@providerid",
                    new SqlParameter("providerid", providerid)).ToList();

                if (objOperatingCountryResponse.opContresponse.countries.Count > 0)
                {
                    objOperatingCountryResponse.opContresponse.code = CommonModel.SuccessCode;
                }
                else if (objOperatingCountryResponse.opContresponse.countries.Count == 0)
                {
                    objOperatingCountryResponse.opContresponse.code = CommonModel.NoRecord;
                }
            }
            catch (Exception ex)
            {
                objOperatingCountryResponse.opContresponse.code = CommonModel.FailureCode;
                objOperatingCountryResponse.opContresponse.code = CommonModel.Description;
            }
            return Json(objOperatingCountryResponse);
        }

        
        [HttpPost]
        [Route("editCountriesList")]
        public IHttpActionResult EditOperatingCountries(EditOperatingCountries model)
        {

            OperatingCountryResponse objOperatingCountryResponse = new OperatingCountryResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 providerid = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                DbContext.Database.ExecuteSqlCommand("delete from OperatingCountries where providerId={0}", providerid);

                foreach (var ctryid in model.countryId)
                {
                    DbContext.Database.ExecuteSqlCommand("insert into OperatingCountries(ProviderID,CountryID) Values({0},{1})", providerid, ctryid);
                }
                objOperatingCountryResponse.opContresponse.countries = DbContext.Database.SqlQuery<OperatingCountryModel>("GetOperatingCountryList @providerid=@providerid",
                    new SqlParameter("providerid", providerid)).ToList();

                if (objOperatingCountryResponse.opContresponse.countries.Count > 0)
                {
                    objOperatingCountryResponse.opContresponse.code = CommonModel.SuccessCode;
                }
                else if (objOperatingCountryResponse.opContresponse.countries.Count == 0)
                {
                    objOperatingCountryResponse.opContresponse.code = CommonModel.NoRecord;
                }
            }
            catch (Exception ex)
            {
                objOperatingCountryResponse.opContresponse.code = CommonModel.FailureCode;
                objOperatingCountryResponse.opContresponse.code = CommonModel.Description;
            }
            return Json(objOperatingCountryResponse);
        }

        
        [HttpPost]
        [Route("setUpRate")]
        public HttpResponseMessage ProvidersetUpRate([FromBody]ProviderRatesModel model)
        {
            ProviderRatesModelResponse objRatesModelResponse = new ProviderRatesModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    int value = DbContext.Database.ExecuteSqlCommand("ProvidersetUpRate @ProviderID=@ProviderID,@title=@title,@DeliveryType=@DeliveryType,@DocumentType=@DocumentType,@heightfrom=@heightfrom,@heightto=@heightto,@weightfrom= @weightfrom, @weightto=@weightto,@widthfrom=@widthfrom,@widthto=@widthto,@lengthfrom=@lengthfrom,@lengthto=@lengthto,@price=@price,@minCharge=@minCharge,@deliveryTime=@deliveryTime",
                         new SqlParameter("ProviderID", ProviderID),
                        new SqlParameter("title", (object)model.title ?? DBNull.Value),
                                     new SqlParameter("DeliveryType", (object)model.DeliveryType ?? DBNull.Value),
                                      new SqlParameter("DocumentType", (object)model.DocumentType ?? DBNull.Value),
                                    new SqlParameter("heightfrom", (object)model.height.HeightFrom ?? DBNull.Value),
                                    new SqlParameter("heightto", (object)model.height.HeightTo ?? DBNull.Value),
                                    new SqlParameter("weightfrom", (object)model.weight.WeightFrom ?? DBNull.Value),
                                    new SqlParameter("weightto", (object)model.weight.WeightTo ?? DBNull.Value),
                                    new SqlParameter("widthfrom", (object)model.width.WidthFrom ?? DBNull.Value),
                                    new SqlParameter("widthto", (object)model.width.WidthTo ?? DBNull.Value),
                                    new SqlParameter("lengthfrom", (object)model.length.LengthFrom ?? DBNull.Value),
                                    new SqlParameter("lengthto", (object)model.length.LengthTo ?? DBNull.Value),
                                    new SqlParameter("price", (object)model.PricePerKG ?? DBNull.Value),
                                    new SqlParameter("minCharge", (object)model.MinimumCharges ?? DBNull.Value),
                                    new SqlParameter("deliveryTime", (object)model.DeliveryDays ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objRatesModelResponse.message = "Inserted Successfully";
                        objRatesModelResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objRatesModelResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objRatesModelResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

        
        [Route("rateList")]
        [HttpPost]
        public IHttpActionResult GetRateList([FromBody]RateObject RO)
        {
            RateObject objRateResponse = new RateObject();
            try
            {
                objRateResponse.response.rates = DbContext.Database.SqlQuery<ProviderRatesModel>("GetRateList").ToList();


                if (objRateResponse.response.rates.Count > 0)
                {
                    objRateResponse.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objRateResponse.response.code = CommonModel.NoRecord;
                }
            }
            catch (Exception ex)
            {

                objRateResponse.response.code = CommonModel.FailureCode;
                objRateResponse.response.code = CommonModel.Description;
            }
            return Json(objRateResponse);
        }


        
        [HttpPost]
        [Route("deleteRate")]
        public HttpResponseMessage DeleteRateList([FromBody]ProviderRatesModel model)
        {
            ProviderRatesModelResponse objRatesModelResponse = new ProviderRatesModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    int value = DbContext.Database.ExecuteSqlCommand("DeleteRateList @ProviderID=@ProviderID,@ID=@ID",
                         new SqlParameter("ProviderID", ProviderID),
                        new SqlParameter("ID", (object)model.id ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objRatesModelResponse.message = "Deleted Successfully";
                        objRatesModelResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objRatesModelResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objRatesModelResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

       
        [HttpPost]
        [Route("changeOrderStatus")]
        public HttpResponseMessage changeOrderStatus([FromBody]EditStausRequest model)
        {
            EditStausResponseModel objStatusResponse = new EditStausResponseModel();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    int value = DbContext.Database.ExecuteSqlCommand("ChangeOrderStatus @ProviderID=@ProviderID,@OrderId=@OrderId,@Status=@Status",
                         new SqlParameter("ProviderID", ProviderID),
                         new SqlParameter("OrderId", model.id),
                        new SqlParameter("Status", (object)model.status ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objStatusResponse.message = "Order Updated Successfully";
                        objStatusResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objStatusResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objStatusResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }


      
        [HttpPost]
        [Route("updateSeriveAndDeliveryInfo")]
        public HttpResponseMessage updateSeriveAndDeliveryInfo([FromBody]ServiceDeliveryRequest model)
        {
            ServiceDeliveryResponseModel objResponse = new ServiceDeliveryResponseModel();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    int value = DbContext.Database.ExecuteSqlCommand("updateSeriveAndDeliveryInfo @ProviderID=@ProviderID,@DeliveryType=@DeliveryType,@DocumentType=@DocumentType,@PriorityType=@PriorityType",
                         new SqlParameter("ProviderID", ProviderID),
                        new SqlParameter("DeliveryType", (object)model.deliveryType ?? DBNull.Value),
                        new SqlParameter("DocumentType", (object)model.documentType ?? DBNull.Value),
                        new SqlParameter("PriorityType", (object)model.priorityType ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objResponse.message = "Details Updated Successfully";
                        objResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

        
        [HttpPost]
        [Route("updateBankDetails")]
        public HttpResponseMessage updateBankDetails([FromBody] PaymentDetailsRequest model)
        {
            PaymentDetailsResponseModel objResponse = new PaymentDetailsResponseModel();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();

                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    int value = DbContext.Database.ExecuteSqlCommand("InsertBankDetails @ProviderID=@ProviderID,@IFSC=@IFSC,@SwiftCode=@SwiftCode,@AccountType=@AccountType,@AccountNumber=@AccountNumber,@IBAN=@IBAN,@CIN=@CIN,@PayPALID=@PayPALID",
                     new SqlParameter("ProviderID", ProviderID),
                     new SqlParameter("IFSC", (object)model.IFSC ?? DBNull.Value),
                     new SqlParameter("SwiftCode", (object)model.SwiftCode ?? DBNull.Value),
                     new SqlParameter("AccountType", (object)model.AccountType ?? DBNull.Value),
                     new SqlParameter("AccountNumber", (object)model.AccountNumber ?? DBNull.Value),
                     new SqlParameter("IBAN", (object)model.IBAN ?? DBNull.Value),
                     new SqlParameter("CIN", (object)model.CIN ?? DBNull.Value),
                     new SqlParameter("PayPALID", (object)model.PayPALID ?? DBNull.Value));

                    if (value > 0)
                    {
                        objResponse.message = "Bank details updated Successfully";
                        objResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }
    }
}
