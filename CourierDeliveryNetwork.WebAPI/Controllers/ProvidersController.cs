﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Providers")]
    public class ProvidersController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

      
        [HttpPost]
        [Route("Registration")]
        public HttpResponseMessage RegisterProviders([FromBody]ProviderRegistrationModel model)
        {
            ProviderRegisterModelResponse objRegisterModelResponse = new ProviderRegisterModelResponse();       
            try
            {
                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string token = Token.GetUniqueToken();

                    int value = DbContext.Database.ExecuteSqlCommand("RegisterProvider @Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Password=@Password,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@Address=@Address,@ServicesOffered=@ServicesOffered,@ServicesLocation=@ServicesLocation,@IsActive=@IsActive,@CreatedDate=@CreatedDate,@DOB=@DOB,@Gender=@Gender,@CompanyRegistrationNo=@CompanyRegistrationNo,@Locality=@Locality,@CompanyLogo=@CompanyLogo,@Description=@Description,@ProviderToken=@ProviderToken",
                                    new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                                    new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                                    new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                                    new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                                    new SqlParameter("Password", (object)model.Password ?? DBNull.Value),
                                    new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                                    new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                                    new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                                    new SqlParameter("Address", (object)model.Address ?? DBNull.Value),
                                    new SqlParameter("ServicesOffered", (object)model.ServicesOffered ?? DBNull.Value),
                                    new SqlParameter("ServicesLocation", (object)model.ServicesLocation ?? DBNull.Value),
                                    new SqlParameter("IsActive", (object)model.IsActive ?? DBNull.Value),
                                    new SqlParameter("CreatedDate", (object)model.CreatedDate ?? DBNull.Value),
                                    new SqlParameter("DOB", (object)model.DOB ?? DBNull.Value),
                                    new SqlParameter("Gender", (object)model.Gender ?? DBNull.Value),
                                    new SqlParameter("CompanyRegistrationNo", (object)model.CompanyRegistrationNo ?? DBNull.Value),
                                    new SqlParameter("Locality", (object)model.Locality ?? DBNull.Value),
                                    new SqlParameter("CompanyLogo", (object)model.CompanyLogo ?? DBNull.Value),
                                    new SqlParameter("Description", (object)model.Description ?? DBNull.Value),
                                    new SqlParameter("ProviderToken", (object)token ?? DBNull.Value)
                                   );

                    if (value > 0)
                    {
                        objRegisterModelResponse.message = "Registered Successfully";
                        objRegisterModelResponse.code = CommonModel.SuccessCode;
                    }
                    else
                    {
                        objRegisterModelResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objRegisterModelResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

       
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage ProviderLogin(ProviderLoginModel model)
        {
            ProviderLoginModelObject objProviderLoginModelObject = new ProviderLoginModelObject();
            try
            {
                objProviderLoginModelObject.response = DbContext.Database.SqlQuery<ProviderLoginModelResponse>("ValidateProvider @Email=@Email,@password=@password",
                        new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                        new SqlParameter("password", (object)model.Password ?? DBNull.Value)).FirstOrDefault();

                if (objProviderLoginModelObject.response != null)
                {
                    objProviderLoginModelObject.response.message = "Login Successfully";
                    objProviderLoginModelObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objProviderLoginModelObject.response.message = "Email Or password does not match";
                    objProviderLoginModelObject.response.code = CommonModel.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, objProviderLoginModelObject);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objProviderLoginModelObject);

        }

       
        [HttpPost]
        [Route("refreshToken")]
        public HttpResponseMessage refreshToken()
        {
            RefreshTokenObject objResponse = new RefreshTokenObject();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 ProviderID = DbContext.Database.SqlQuery<Int64>("select providerId from UserToken where Token={0}", token).FirstOrDefault();
                using (CourierNetwork1Context DbContext = new CourierNetwork1Context())
                {
                    string Newtoken = Token.GetUniqueToken();

                    int value = DbContext.Database.ExecuteSqlCommand("GenerateNewToken @ID=@ID,@TokenFor=@TokenFor,@Newtoken=@Newtoken",
                                    new SqlParameter("@ID", ProviderID),
                                    new SqlParameter("TokenFor", "Provider"),
                                    new SqlParameter("Newtoken", Newtoken)
                                   );

                    if (value > 0)
                    {
                        objResponse.message = "Token Refreshed Successfully!!";
                        objResponse.code = CommonModel.SuccessCode;
                        objResponse.userToken = Newtoken;

                    }
                    else
                    {
                        objResponse.message = "Some Error Occured";
                    }
                    var message = Request.CreateResponse(HttpStatusCode.Created, objResponse);
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }
    }
}