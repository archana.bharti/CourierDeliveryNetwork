﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.ProvidersOrderHistoryModel;
using static CourierDeliveryNetwork.WebAPI.Models.ProvidersOrderListModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/Provider")]
    public class ProvidersOrderHistoryController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        [HttpPost]
        [Route("history")]
        [SecureResource]
        public HttpResponseMessage ProviderOrderHistory(ProvidersOrderHistoryResponse objreq)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 providerid = DbContext.Database.SqlQuery<Int64>("Select providerid from UserToken where Token={0}", token).FirstOrDefault();
                ProvidersOrderHistoryObject objresponse = new ProvidersOrderHistoryObject();

                objresponse.response.total = DbContext.Database.SqlQuery<Int32>("GetProviderOrderHistory @TypeInfo=@TypeInfo,@ProviderId=@ProviderId,@Limit=@Limit,@Offset=@Offset",
                                                          new SqlParameter("@TypeInfo", "Count"),
                                                          new SqlParameter("ProviderId", providerid),
                                                          new SqlParameter("Limit", objreq.limit),
                                                          new SqlParameter("Offset", objreq.offset)).FirstOrDefault();

                objresponse.response.providersOrder_history = DbContext.Database.SqlQuery<ProvidersOrderHistoryData>("GetProviderOrderHistory @TypeInfo=@TypeInfo,@ProviderId=@ProviderId,@Limit=@Limit,@Offset=@Offset",
                                                         new SqlParameter("@TypeInfo", "Detail"),
                                                         new SqlParameter("ProviderId", providerid),
                                                         new SqlParameter("Limit", objreq.limit),
                                                         new SqlParameter("Offset", objreq.offset)).ToList();

                if (objresponse.response.providersOrder_history.Count>0)
                {
                    objresponse.response.code = CommonModel.SuccessCode;
                    objresponse.response.limit = objreq.limit;
                    objresponse.response.offset = objreq.offset;
                }
                else
                {
                    objresponse.response.code = CommonModel.BadRequest; 
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objresponse);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("OrderList")]
        [SecureResource]
        public HttpResponseMessage ProviderOrderList(ProviderOrderListResponse obj)
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();
                Int64 providerid = DbContext.Database.SqlQuery<Int64>("Select providerid from UserToken where Token={0}", token).FirstOrDefault();

                Int32 TotalOrders = DbContext.Database.SqlQuery<Int32>("Select Count(*) from OrderFinal ORF where providerid={0}", providerid).FirstOrDefault();

                var RequestedOrders = DbContext.Database.SqlQuery<Int64>("GetAllProvidersOrders @Info=@Info,@providerid=@providerid,@datefrom=@datefrom,@dateto=@dateto,@offset=@offset,@limit=@limit",
                                               new SqlParameter("Info", "Orders"),
                                               new SqlParameter("providerid", providerid),
                                               new SqlParameter("datefrom", obj.filter.datefrom),
                                               new SqlParameter("dateto", obj.filter.dateto),
                                               new SqlParameter("offset", obj.offset),
                                               new SqlParameter("limit", obj.limit)
                                               ).ToList();


                ProviderOrderListObject Resmodel = new ProviderOrderListObject();
                foreach (var orderid in RequestedOrders)
                {
                    //for provider details
                    ProviderOrderListData obj1 = new ProviderOrderListData();
                    obj1.customerDetails = DbContext.Database.SqlQuery<CustomerDetails>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "customerDetails")
                                                   ).FirstOrDefault();
                   
                    //for order details
                    obj1.orderDetails = DbContext.Database.SqlQuery<OrderDetails>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "orderDetails")
                                                    ).FirstOrDefault();

                    //for currency
                    obj1.orderDetails.currency = DbContext.Database.SqlQuery<Currency>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "currency")
                                                    ).FirstOrDefault();

                    //for PaymentDetails
                    obj1.paymentDetails = DbContext.Database.SqlQuery<PaymentDetails>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "paymentDetails")
                                                    ).FirstOrDefault();

                    //for PickUp
                    obj1.pickUp = DbContext.Database.SqlQuery<PickUp>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "pickUp")
                                                    ).FirstOrDefault();

                    //for dropOff
                    obj1.dropOff = DbContext.Database.SqlQuery<DropOff>("GetProviderOrderList @Orderid=@Orderid,@TypeofInfo=@TypeofInfo",
                                                    new SqlParameter("Orderid", orderid),
                                                    new SqlParameter("TypeofInfo", "dropOff")
                                                    ).FirstOrDefault();
                    Resmodel.response.providerOrder_List.Add(obj1);
                }
                if (Resmodel != null)
                {
                    Resmodel.response.code = Convert.ToInt32(CommonModel.SuccessCode);
                    Resmodel.response.offset = Convert.ToInt32(obj.offset);
                    Resmodel.response.limit = Convert.ToInt32(obj.limit);
                    Resmodel.response.total = Convert.ToInt32(TotalOrders);
                }
                else
                {
                    Resmodel.response.code = Convert.ToInt32(CommonModel.BadRequest);
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, Resmodel);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
