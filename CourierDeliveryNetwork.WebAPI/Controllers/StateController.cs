﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Models;
using System.Data.SqlClient;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/State")]
    public class StateController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();

        // Post API for State List
        
        [Route("States")]
        [HttpPost]
        public IHttpActionResult GetStatesList([FromBody]StateResponse SO)
        {
            StateObject objStateResponse = new StateObject();
            try
            {
              
                objStateResponse.response.states = DbContext.Database.SqlQuery<StateModel>("GetStateList @CountryId=@CountryId,@Offset=@Offset,@Limit=@Limit,@SearchKey=@SearchKey",
                                                   new SqlParameter("CountryId", SO.CountryId),
                                                   new SqlParameter("Offset", SO.offset),
                                                   new SqlParameter("Limit", SO.limit),
                                                   new SqlParameter("SearchKey", SO.SearchKey)).ToList();
               

                if (objStateResponse.response.states.Count>0)
                {
                    objStateResponse.response.code = CommonModel.SuccessCode;
                   
                }
                else
                {
                    objStateResponse.response.code = CommonModel.NoRecord;
                }
            }
            catch (Exception ex)
            {

                objStateResponse.response.code = CommonModel.FailureCode;
                objStateResponse.response.code = CommonModel.Description;
            }

            return Json(objStateResponse);
        }


        //Post method for city list according to state
        
        [Route("Cities")]
        [HttpPost]
        public IHttpActionResult GetCityList(CityModelResponse model)
        {
            CityModelObject objCityModelObject = new CityModelObject();
            try
            {
                objCityModelObject.response.Cities = DbContext.Database.SqlQuery<CityModel>("GetCityList @CountryId=@CountryId,@StateId=@StateId,@Offset=@Offset,@Limit=@Limit,@SearchKey=@SearchKey",
                                                   new SqlParameter("CountryId", model.CountryId),
                                                   new SqlParameter("StateId", model.@StateId),
                                                   new SqlParameter("Offset", model.offset),
                                                   new SqlParameter("Limit", model.limit),
                                                   new SqlParameter("SearchKey", model.SearchKey)).ToList();

                if (objCityModelObject.response.Cities.Count>0)
                {
                    objCityModelObject.response.code = CommonModel.SuccessCode;
                    objCityModelObject.response.offset = model.offset;
                    objCityModelObject.response.limit = model.limit;
                }
                else
                {
                    objCityModelObject.response.code = CommonModel.BadRequest;
                }
               
            }
             catch(Exception ex)
            {
                objCityModelObject.response.code = CommonModel.FailureCode;
                objCityModelObject.response.code = CommonModel.Description;
            }
            return Json(objCityModelObject);
        }

       
    }
    }

