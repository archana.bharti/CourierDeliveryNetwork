﻿using CourierDeliveryNetwork.Data;
using CourierDeliveryNetwork.WebAPI.Filter;
using CourierDeliveryNetwork.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CourierDeliveryNetwork.WebAPI.Models.UserModel;

namespace CourierDeliveryNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/User")]
    public class UserProfileController : ApiController
    {
        CourierNetwork1Context DbContext = new CourierNetwork1Context();


       
        [HttpPost]
        [Route("ForgetPassword")]
        public HttpResponseMessage UserForgetPassword(UserForgetPassword model)
        {
            
            try
            {
                UserForgetPasswordObject objUserForgetPasswordObject = new UserForgetPasswordObject();
                int value = DbContext.Database.ExecuteSqlCommand("CustomerForgetPassword @Email=@Email,@Password=@Password",
                      new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                      new SqlParameter("Password", (object)model.Password ?? DBNull.Value));
                
                if(value>0)
                {
                    objUserForgetPasswordObject.response.message = "Password changed Successfully";
                    objUserForgetPasswordObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objUserForgetPasswordObject.response.code = CommonModel.BadRequest;
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objUserForgetPasswordObject);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

       
        [HttpPost]
        [Route("ContactUs")]
        public HttpResponseMessage UserContactUs(UserContactUs model)
        {
            try
            {
                UserContactUsResponse objUserContactUsResponse = new UserContactUsResponse();

                int value = DbContext.Database.ExecuteSqlCommand("SaveEnquiry @Name=@Name,@Email=@Email,@ContactNo=@ContactNo,@Message=@Message,@Date=@Date",
                                                                 //new SqlParameter("EnquiryID", (object)model.EnquiryID ?? DBNull.Value),
                                                                 new SqlParameter("Name", (object)model.Name ?? DBNull.Value),
                                                                 new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                                                                 new SqlParameter("ContactNo", (object)model.ContactNo ?? DBNull.Value),
                                                                 new SqlParameter("Message", (object)model.Message ?? DBNull.Value),
                                                                 new SqlParameter("Date", (object)model.Date ?? DBNull.Value)

                                                                 );
                if(value>0)
                {
                    objUserContactUsResponse.message = "Your Enquiry will be resolved in 24 hours";
                    objUserContactUsResponse.code = CommonModel.SuccessCode;
                    
                }
                else
                {
                    objUserContactUsResponse.code = CommonModel.BadRequest;
                    objUserContactUsResponse.message = "Some Error Occured";
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objUserContactUsResponse);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


       
        [HttpPost]
        [Route("Categories")]
        public HttpResponseMessage UserCategories(UserCategories model)
        {


           
            UserCategoriesRootObject objUserCategoriesRootObject = new UserCategoriesRootObject();
            try
            {
                objUserCategoriesRootObject.response.categories = DbContext.Database.SqlQuery<UserCategories>("GetCategoryList").ToList();
                if(objUserCategoriesRootObject.response.categories.Count>0)
                {
                    objUserCategoriesRootObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objUserCategoriesRootObject.response.code = CommonModel.BadRequest;
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objUserCategoriesRootObject);
                return message;
            }
            
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

       
        [HttpPost]
        [Route("UserDetails")]
        [SecureResource]
        public HttpResponseMessage UserDetailInfo()
        {
            UserDetailObject objUserDetailObject = new UserDetailObject();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token= headers.Authorization.Parameter.ToString();

                
                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                UserDetail model = new UserDetail();
                model = DbContext.Database.SqlQuery<UserDetail>("GetCustomerDetail @UserId=@UserId",
                    new SqlParameter("UserId", UserId)).FirstOrDefault();
               
                if(model!=null)
                {
                    objUserDetailObject.response.code = CommonModel.SuccessCode;

                }
                else
                {
                    objUserDetailObject.response.code = CommonModel.BadRequest;

                }

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

       
        [HttpPost]
        [Route("UpdateUserDetails")]
        [SecureResource]
        public HttpResponseMessage EditUserDetails(CustomerRegistrationModel model)
        {
            try
            {
                EditUserDetailObject objEditUserDetailObject = new EditUserDetailObject();
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                CustomerRegistrationModel model1 = new CustomerRegistrationModel();


                int value = DbContext.Database.ExecuteSqlCommand("EditCustomerDetails @Email=@Email,@FirstName=@FirstName,@LastName=@LastName,@PhoneNo=@PhoneNo,@Password=@Password,@CountryId=@CountryId,@StateId=@StateId,@PostalCode=@PostalCode,@SocialId=@SocialId,@DOB=@DOB",
                         //new SqlParameter("CustomerID",UserId),
                         new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                         new SqlParameter("FirstName", (object)model.FirstName ?? DBNull.Value),
                         new SqlParameter("LastName", (object)model.LastName ?? DBNull.Value),
                         new SqlParameter("PhoneNo", (object)model.PhoneNo ?? DBNull.Value),
                         new SqlParameter("Password", (object)model.Password ?? DBNull.Value),
                         new SqlParameter("CountryId", (object)model.CountryId ?? DBNull.Value),
                         new SqlParameter("StateId", (object)model.StateId ?? DBNull.Value),
                         new SqlParameter("PostalCode", (object)model.PostalCode ?? DBNull.Value),
                         new SqlParameter("SocialId", (object)model.SocialId ?? DBNull.Value),
                        new  SqlParameter("DOB", (object)model.DOB ?? DBNull.Value)
                         );

                if (value > 0)
                {
                    objEditUserDetailObject.response.code = CommonModel.SuccessCode;
                    objEditUserDetailObject.response.message = "Your Information is updated successfully";

                }
                else
                {
                    objEditUserDetailObject.response.code = CommonModel.BadRequest;

                }

                var message = Request.CreateResponse(HttpStatusCode.Created, objEditUserDetailObject);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


       
        [HttpPost]
        [Route("ChangePassword")]
        [SecureResource]
        public HttpResponseMessage UserChangePassword(UserChangePasswordModel model)
        {
            UserChangePasswordModelResponse objUserChangePasswordModelResponse = new UserChangePasswordModelResponse();
            try
            {
                var re = Request;
                var headers = re.Headers;
                string token = headers.Authorization.Parameter.ToString();

                Int64 UserId = DbContext.Database.SqlQuery<Int64>("select userId from UserToken where Token={0}", token).FirstOrDefault();

                int value = DbContext.Database.ExecuteSqlCommand("CustomerChangePassword @Email=@Email,@Password=@Password",
                            new SqlParameter("Email", (object)model.Email ?? DBNull.Value),
                            new SqlParameter("Password", (object)model.Password ?? DBNull.Value));
                if(value>0)
                {
                    objUserChangePasswordModelResponse.code = CommonModel.SuccessCode;
                }
                else
                {
                    objUserChangePasswordModelResponse.code = CommonModel.BadRequest;
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objUserChangePasswordModelResponse);
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

      
        [HttpPost]
        [Route("GetContactInfo")]
        public HttpResponseMessage ContactInfoModel(ContactInfoModel model)
        {
            ContactInfoModelObject objContactInfoModelObject = new ContactInfoModelObject();
            try
            {
                objContactInfoModelObject.response.contactinfolist = DbContext.Database.SqlQuery<ContactInfoModel>("GetContactDetails").ToList();


                if(objContactInfoModelObject.response.contactinfolist.Count>0)
                {
                    objContactInfoModelObject.response.code = CommonModel.SuccessCode;
                }
                else
                {
                    objContactInfoModelObject.response.code = CommonModel.BadRequest;
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, objContactInfoModelObject);
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            
        }
    }
}
