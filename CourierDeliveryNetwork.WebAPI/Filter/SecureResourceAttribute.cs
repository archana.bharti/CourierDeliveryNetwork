﻿using CourierDeliveryNetwork.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using static CourierDeliveryNetwork.WebAPI.Models.UserModel;

namespace CourierDeliveryNetwork.WebAPI.Filter
{
   
    public class SecureResourceAttribute: AuthorizationFilterAttribute
    {
       
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var authorizeHeader = actionContext.Request.Headers.Authorization;
            // && authorizeHeader.Scheme.Equals("bearer ", StringComparison.OrdinalIgnoreCase)
            if (authorizeHeader != null && String.IsNullOrEmpty(authorizeHeader.Parameter) == false)
            {
                
                CourierNetwork1Context DbContext = new CourierNetwork1Context();
                var existingToken = DbContext.Database.SqlQuery<UserTokenModel>("ValidateUserToken @Token=@Token",
                     new SqlParameter("Token", authorizeHeader.Parameter)).FirstOrDefault();


               // var existingToken = DbContext.UserTokens.Where(x => x.Token == authorizeHeader.Parameter).FirstOrDefault();



                if (existingToken != null)
                {

                    var principal = new GenericPrincipal((new GenericIdentity(existingToken.UserId.ToString())),
                                                                    (new[] { existingToken.Id.ToString() }));
                    Thread.CurrentPrincipal = principal;
                    if (HttpContext.Current != null)
                        HttpContext.Current.User = principal;

                    return;

                }
            }
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);

            actionContext.Response.Content = new StringContent("The Token is not Valid");
        }
    }
}