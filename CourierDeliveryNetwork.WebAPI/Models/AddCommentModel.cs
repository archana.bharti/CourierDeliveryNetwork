﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class AddCommentModel
    {

        
        public class AddCommentReq
        {
            public Int64 UserId { get; set; }
            public int ID { get; set; }
            public int RequirementID { get; set; }
            public string Comment { get; set; }
            public DateTime CreatedDate { get; set; }
        }
        public class AddComment
        {
            public int RequirementID { get; set; }
            public string Comment { get; set; }
            public DateTime CreatedDate { get; set; }
        }



        public class AddCommentResponse
        {
            public string code { get; set; }
       
            public List<AddComment> comment { get; set; }
            public AddCommentResponse()
            {
                comment = new List<AddComment>();
            }
        }

        public class AddCommentObject
        {
            public AddCommentResponse response { get; set; }
            public AddCommentObject()
            {
                response = new AddCommentResponse();
            }
        }
    }
}