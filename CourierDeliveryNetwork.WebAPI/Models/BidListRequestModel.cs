﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class BidListRequestModel
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int RequirementID { get; set; }
    }
}