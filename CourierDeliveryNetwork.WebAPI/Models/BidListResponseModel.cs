﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class BidListResponseModel
    {
        public BidListResponseModel()
        {
            response = new BidListResponse();
        }
        public BidListResponse response { get; set; }
    }

    public class BidListResponseCurrency
    {
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
    }

    public class BidDetails
    {
        public int RequirementID { get; set; }
        public int BidID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Decimal Amount { get; set; }
        public Int64 CustomerID { get; set; }
        public string CurrencyCode { get; set; }
        public BidListResponseCurrency currency { get; set; }
    }

    public class BidListUserDetails
    {
        public string Name { get; set; }
        
        public string ProfilePic { get; set; }
    }

    public class BidListData
    {
        public BidListData()
        {
            bidDetails = new BidDetails();
            userDetails = new BidListUserDetails();
        }
        public BidDetails bidDetails { get; set; }
        public BidListUserDetails userDetails { get; set; }
    }

    public class BidListResponse
    {
        public BidListResponse()
        {
            data = new List<BidListData>();
        }
        public string code { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public List<BidListData> data { get; set; }
    }
}