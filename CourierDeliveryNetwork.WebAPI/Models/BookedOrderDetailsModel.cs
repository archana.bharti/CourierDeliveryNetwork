﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class BookedOrderDetailsModel
    {
        public class OrderDetailsRequestModel
        {
            public int orderId { get; set; }
        }
        
        public class CustomerDetails
        {
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string address { get; set; }
        }

        public class PickUp
        {
            public string contactDetails { get; set; }
            public string address { get; set; }
        }

        public class DropOff
        {
            public string contactDetails { get; set; }
            public string address { get; set; }
        }

        public class PackageDetails
        {
            public int height { get; set; }
            public int weight { get; set; }
            public int width { get; set; }
            public int length { get; set; }
            public string deliveryType { get; set; }
            public string documentType { get; set; }
            public int itemValue { get; set; }
        }

        public class Currency
        {
            public string name { get; set; }
            public string id { get; set; }
        }

        public class OrderDetails
        {
            public OrderDetails()
            {
                currency = new Currency();
            }
            public Int64 orderId { get; set; }
            public int amountTobePaid { get; set; }
            public string status { get; set; }
            public Currency currency { get; set; }
        }

        public class PaymentDetails
        {
            public Int64 transactionId { get; set; }
            public decimal serviceTax { get; set; }
            public decimal vat { get; set; }
            public decimal discount { get; set; }
            public int totalPayble { get; set; }
            public string status { get; set; }
        }

        public class OrderResponse
        {

            public OrderResponse()
            {
                customerDetails = new CustomerDetails();
                pickUp = new PickUp();
                dropOff = new DropOff();
                packageDetails = new PackageDetails();
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
            }
            public int code { get; set; }
            public CustomerDetails customerDetails { get; set; }
            public PickUp pickUp { get; set; }
            public DropOff dropOff { get; set; }
            public PackageDetails packageDetails { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
            public string description { get; set; }
        }

        public class MainOrderResponseObject
        {
            public MainOrderResponseObject()
            {
                response = new OrderResponse();
            }
            public OrderResponse response { get; set; }
        }
    }
}