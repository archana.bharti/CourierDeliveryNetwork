﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CPOrderHistoryModel
    {
        public class CPOrderHistoryRequestDate
        {
            public DateTime from { get; set; }
            public DateTime to { get; set; }
        }

        public class OrderHistoryRequestModel
        {
            public OrderHistoryRequestModel()
            {
                requestdate = new CPOrderHistoryRequestDate();
            }
            public CPOrderHistoryRequestDate requestdate { get; set; }
            public string filterBy { get; set; }
            public Int64 offset { get; set; }
            public Int64 limit { get; set; }
        }

        public class Customer
        {
            public string name { get; set; }
            public string address { get; set; }
            public string email { get; set; }
        }

        public class From
        {
            public string houseStreet { get; set; }
            public string landMark { get; set; }
            public Int32 countryId { get; set; }
            public Int32 stateId { get; set; }
            public string postalCode { get; set; }
        }

        public class To
        {
            public string houseStreet { get; set; }
            public string landMark { get; set; }
            public Int32 countryId { get; set; }
            public Int32 stateId { get; set; }
            public string postalCode { get; set; }
        }

        public class Currency
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        public class OrderDetails
        {
            public Int64 orderId { get; set; }
            public int amountTobePaid { get; set; }
            public string status { get; set; }
            public Currency currency { get; set; }
        }

        public class PaymentDetails
        {
            public decimal serviceTax { get; set; }
            public decimal vat { get; set; }
            public decimal discount { get; set; }
            public int totalPayble { get; set; }
            public string status { get; set; }
        }

        public class Orders
        {
            public Orders()
            {
                customer = new Customer();
                from = new From();
                to = new To();
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
                currency = new Currency();
            }
            public Customer customer { get; set; }
            public From from { get; set; }
            public To to { get; set; }
            public string description { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
            public Currency currency { get; set; }
        }

        public class OrdersResponse
        {
            public OrdersResponse()
                {
                data = new List<Orders>();
                }
            public int code { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
            public List<Orders> data { get; set; }
        }

        public class MainOrderHistoryResponseObject
        {
            public MainOrderHistoryResponseObject()
            {
                response = new OrdersResponse();
            }
            public OrdersResponse response { get; set; }
        }
    }
}