﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{

    public class Company
    {
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyLogo { get; set; }
        public string TIN_VAT { get; set; }
    }

    public class CarrierProviderModel
    {
        public Int64 CarrierProviderId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }

        public int CountryId { get; set; }
        public string countryName { get; set; }
        public int StateId { get; set; }
        public string stateName { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }

        public Company company { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
                
        public string Description { get; set; }
        public string CarrierProviderToken { get; set; }
    }

    public class CarrierProviderModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }


    public class CarrierProviderLoginModel
    {
        public int DeviceId { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceOsversion { get; set; }
        public string SocialId { get; set; }
        public string Email { get; set; }
    }

    public class LoginResponseData
    {
        public Int64 CarrierProviderId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ZipCode { get; set; }
        public string Token { get; set; }
    }

    public class CarrierProviderLoginResponse
    {
        public CarrierProviderLoginResponse()
        {
            data = new LoginResponseData();
        }
        public string code { get; set; }
        public LoginResponseData data { get; set; }
        public string token { get; set; }
        public string message { get; set; }
    }

    public class LoginMainResponse
    {
        public LoginMainResponse()
        {
            response = new CarrierProviderLoginResponse();
        }
        public CarrierProviderLoginResponse response { get; set; }
    }

    public class CarrierProviderForgetPwd
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
    public class CarrierProviderForgetPwdResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }
    public class CarrierProviderForgetPasswordObject
    {
        public CarrierProviderForgetPasswordObject()
        {
            response = new CarrierProviderForgetPwdResponse();
        }
        public CarrierProviderForgetPwdResponse response { get; set; }
    }

    public class CarProviderDetailResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public CarrierProviderModel carProviderDetails { get; set; }
    }

    public class CarProviderResponseObject
    {
        public CarProviderResponseObject()
        {
            response = new CarProviderDetailResponse();
        }
        public CarProviderDetailResponse response { get; set; }
    }
}