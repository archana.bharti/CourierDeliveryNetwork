﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CartListModel
    {

        public class ProviderDetails
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public Int64 ProviderId { get; set; }
        }

        public class Currency
        {
            public string CurrencyCode { get; set; }
            public string CurrencyName { get; set; }
        }

        public class OrderDetails
        {
            public Int64 OrderID { get; set; }
            public int TotalPayable { get; set; }
            public Currency currency { get; set; }
            public DateTime OrderDate { get; set; }
            public OrderDetails()
            {
                currency = new Currency();

            }
        }

        public class PaymentDetails
        {
            public decimal ServiceTax { get; set; }
            public decimal VAT { get; set; }
            public decimal Discount { get; set; }
            public int TotalPayable { get; set; }
          
        }

        public class PickUp
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }
        }

        public class DropOff
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }
        }

        public class Dimensions
        {
            public int Height { get; set; }
            public int Width { get; set; }
            public int Length { get; set; }
            public int Weight { get; set; }
        }

        public class ParcelDetails
        {
            public int DeliveryType { get; set; }
            public int DocumentType { get; set; }
            public int ItemValue { get; set; }
            public string Description { get; set; }
            public Dimensions dimensions { get; set; }
            public ParcelDetails()
            {
                dimensions = new Dimensions();
            }
        }

        public class CartListMainModel
        {
            public ProviderDetails providerDetails { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
            public PickUp pickUp { get; set; }
            public DropOff dropOff { get; set; }
            public ParcelDetails parcelDetails { get; set; }

            public CartListMainModel()
            {
                providerDetails = new ProviderDetails();
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
                pickUp = new PickUp();
                dropOff = new DropOff();
                parcelDetails = new ParcelDetails();


            }
        }

        public class CartListMainModelResponse
        {
             
            public int code { get; set; }
            public List<CartListMainModel> cartList { get; set; }
            public string message { get; set; }
            public CartListMainModelResponse()
            {
                cartList = new List<CartListMainModel>();
            }
        }

        public class CartListMainModelObject
        {
            public CartListMainModelResponse response { get; set; }
            public CartListMainModelObject()
            {
                response = new CartListMainModelResponse();
            }
        }

    }
}