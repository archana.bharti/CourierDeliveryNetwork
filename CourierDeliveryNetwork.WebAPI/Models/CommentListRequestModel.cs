﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CommentListRequestModel
    {
        public string offset { get; set; }
        public string limit { get; set; }
        public string RequirmentId { get; set; }
        public string searchKey { get; set; }
    }
}