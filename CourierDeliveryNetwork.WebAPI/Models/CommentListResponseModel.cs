﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CommentListResponseModel
    {
       public  CommentListResponseModel()
        {
            response = new CommentListResponse();
        }
        public CommentListResponse response { get; set; }
    }

    public class Comment
    {
       // public Int32 id { get; set; }
        public string message { get; set; }
        public string date { get; set; }
        public Int32 ID { get; set; }
        public Int64 CustomerID { get; set; }
    }

    public class CommentListResponseUserDetails
    {
        public string name { get; set; }
        public string id { get; set; }
        public string imageUrl { get; set; }
    }

    public class CommentListData
    {
        public CommentListData()
        {
            comment = new Comment();
            userDetails = new CommentListResponseUserDetails();
        }
        public Comment comment { get; set; }
        public CommentListResponseUserDetails userDetails { get; set; }
    }

    public class CommentListResponse
    {
        public CommentListResponse()
        {
            data = new List<CommentListData>();
        }
        public string code { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public List<CommentListData> data { get; set; }
    }
}