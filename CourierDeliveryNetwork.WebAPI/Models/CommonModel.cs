﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CommonModel
    {

        public static string SuccessCode = "200";
        public static string NoRecord = "201";
        public static string BadRequest = "Something Bad happend";
        public static string FailureMessage = "Error Occurred, Please try again later.";
        public static string FailureCode = "501";
        public static string LimitExceed = "204";
        public static string AlreadyExist = "409";
        public static string Description = "More details about the error is here";
        public static string DoesNotExist = "The Email id does not exist";
    }
}