﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CountryModel
    {

        public string CountryName { get; set; }
        public string Flag { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
      

    }
        public class Response
        {

        public Response()
        {
            countries = new List<CountryModel>();
        }
            public string code { get; set; }
        public string message { get; set; }
        public List<CountryModel> countries { get; set; }
        }

        public class CountryResponse
        {
        public CountryResponse()
        {
            response = new Response();
        }
            public Response response { get; set; }
        }

    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
