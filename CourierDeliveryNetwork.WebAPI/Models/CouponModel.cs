﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CouponModel
    {
        public class CustomerCouponModel
        {
            public DateTime ValidUpto { get; set; }
            public decimal Discount { get; set; }
        }

        public class CustomerCouponModelResponse
        {
            public string code { get; set; }
            public CustomerCouponModel coupon { get; set; }
            public string message { get; set; }

            public CustomerCouponModelResponse()
            {
                coupon = new CustomerCouponModel();
            }
        }

        public class CustomerCouponModelObject
        {
            public CustomerCouponModelResponse response { get; set; }

            public CustomerCouponModelObject()
            {
                response = new CustomerCouponModelResponse();
            }
        }
    }
}