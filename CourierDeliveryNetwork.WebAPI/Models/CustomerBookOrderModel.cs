﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CustomerBookOrderModel
    {
        public class CustomerBookOrderModelResponse
        {
            public int code { get; set; }
            public string message { get; set; }
        }

        public class CustomerBookOrderResponseMainModel
        {
            public CustomerBookOrderModelResponse response { get; set; }
        }



        public class CustomerBookOrderModelRequest
        {
            public class Dimensions
            {
                public int Height { get; set; }
                public int Width { get; set; }
                public int Length { get; set; }
                public int Weight { get; set; }
            }

            public class PickUp1
            {
                public string StreetAddress { get; set; }
                public string Landmark { get; set; }
                public int Country { get; set; }
                public int State { get; set; }
                public string Pincode { get; set; }
            }

            public class DropOff1
            {
                public string StreetAddress { get; set; }
                public string Landmark { get; set; }
                public int Country { get; set; }
                public int State { get; set; }
                public string Pincode { get; set; }
            }

            public class ParcelDetails
            {
                public string code { get; set; }
                public int DeliveryType { get; set; }
                public int DocumentType { get; set; }
                public int ItemValue { get; set; }
                public string Description { get; set; }
                public Dimensions dimensions { get; set; }
                public PickUp1 pickUp { get; set; }
                public DropOff1 dropOff { get; set; }
                public ParcelDetails()
                {
                    dimensions = new Dimensions();
                    pickUp = new PickUp1();
                    dropOff = new DropOff1();
                }
            }

            public class Amount
            {
                public decimal ServiceTax { get; set; }
                public decimal VAT { get; set; }
                public decimal Discount { get; set; }
                public int TotalPayable { get; set; }
            }

            public class PersonalDetails
            {
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public int Gender { get; set; }
                public string Email { get; set; }
                public string Mobile { get; set; }
                public int CountryID { get; set; }
                public int StateID { get; set; }
                public int CityID { get; set; }
                public string Pincode { get; set; }
                public string Current_Address { get; set; }
            }

            public class BookOrderModelResponse
            {
                public ParcelDetails parcelDetails { get; set; }
                public string CouponCode { get; set; }
                public Amount Amount { get; set; }
                public PersonalDetails personalDetails { get; set; }

                public BookOrderModelResponse()
                {
                    parcelDetails = new ParcelDetails();
                    Amount = new Amount();
                    personalDetails = new PersonalDetails();
                }
            }

            public class CustomerBookOrderResponseMainModel
            {

                public BookOrderModelResponse response { get; set; }
                public CustomerBookOrderResponseMainModel()
                {
                    response = new BookOrderModelResponse();
                }
            }

        }

        public class OrderBookResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }
    }
}