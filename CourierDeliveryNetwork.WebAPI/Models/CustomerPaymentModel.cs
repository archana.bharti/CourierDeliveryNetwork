﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CustomerPaymentModel
    {
        public class Currency
        {
            public string CurrencyCode { get; set; }
            public string CurrencyName { get; set; }
        }

        public class OrderDetails
        {
            public Int64 OrderID { get; set; }
            public int TotalPayable { get; set; }
            public int Status { get; set; }
            public String StatusValue { get; set; }
            public Currency currency { get; set; }
            
            public OrderDetails()
            {
                currency = new Currency();
            }
        }

        public class PaymentDetails
        {
            public Int64 TransactionID { get; set; }
            public decimal ServiceTax { get; set; }
            public decimal VAT { get; set; }
            public decimal Discount { get; set; }
            public int TotalPayable { get; set; }
            public int Status { get; set; }
        }

        public class PickUp
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }

        }

        public class DropOff
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }
        }

        public class Dimensions
        {
            public int Height { get; set; }
            public int Width { get; set; }
            public int Length { get; set; }
            public int Weight { get; set; }
        }

        public class ParcelDetails
        {
            public int DeliveryType { get; set; }
            public int DocumentType { get; set; }
            public int ItemValue { get; set; }
            public string Description { get; set; }
            public Dimensions dimensions { get; set; }

            public ParcelDetails()
            {
                dimensions = new Dimensions();
            }
        }

        public class OrdersList
        {
            //tracking number as OrderID
            public Int64 OrderID { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
            public PickUp pickUp { get; set; }
            public DropOff dropOff { get; set; }
            public ParcelDetails parcelDetails { get; set; }

            public OrdersList()
            {
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
                pickUp = new PickUp();
                dropOff = new DropOff();
                parcelDetails = new ParcelDetails();
            }
        }

        public class CustomerPaymentDetailModel
        {
            public int EarnedPoints { get; set; }
            public List<OrdersList> ordersList { get; set; }

            public CustomerPaymentDetailModel()
            {
                ordersList = new List<OrdersList>();
            }
        }

        public class CustomerPaymentDetailResponse
        {
            public string code { get; set; }
            public CustomerPaymentDetailModel paymentData { get; set; }
            public string message { get; set; }

            public CustomerPaymentDetailResponse()
            {
                paymentData = new CustomerPaymentDetailModel();
            }
        }

        public class CustomerPaymentDetailObject
        {
            public CustomerPaymentDetailResponse response { get; set; }

            public CustomerPaymentDetailObject()
            {
                response = new CustomerPaymentDetailResponse();
            }
        }

        public class CustomerMakePaymentRequest
        {
            public Int64 OrderID { get; set; }
            public Int64 TransactionID { get; set; }
            public string PaymentType { get; set; }
            public string CurrencyCode { get; set; }

             
        }
    }
}