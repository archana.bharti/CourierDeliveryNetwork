﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CustomerPostRequirementModel
    {

        //public class Currency
        //{
        //    public string CurrencyCode { get; set; }
        //    public string CurrencyName { get; set; }
        //}

        public class PostReqirementModel
        {
            public int Category { get; set; }
            public int RequirementID { get; set; }
            public string Title { get; set; }
            public decimal MinimumAmount { get; set; }
            //public string postDate { get; set; }
            public DateTime ValidTill { get; set; }
            public string Description { get; set; }
            //public string Images { get; set; }
            public string Status { get; set; }
            public string CurrencyCode { get; set; }
            public List<string> thumbURL { get; set; }
            public List<string> imagesURL { get; set; }

            public PostReqirementModel()
            {
                thumbURL = new List<string>();
                imagesURL = new List<string>();
                     
                     
            }
        }

        public class PostRequrimentResponse
        {
            public string code { get; set; }
            public PostReqirementModel postReq_List { get; set; }
        }

        public class PostRequrimentObject
        {
            public PostRequrimentResponse response { get; set; }

            public PostRequrimentObject()
            {
                response = new PostRequrimentResponse();
            }
        }
        public class CustomerRequestRequirementModel
        {

            public CustomerRequestRequirementModel()
            {
                ImageUrl = new List<string>();

                ThumbUrl = new List<string>();
            }
            public int RequirementID { get; set; }
            public int Category { get; set; }
            public string Title { get; set; }
            public decimal MinimumAmount { get; set; }
            public DateTime ValidTill { get; set; }
            public string Description { get; set; }
            public List<string> ImageUrl { get; set; }
            public List<string> ThumbUrl { get; set; }
        }
       

    }
}