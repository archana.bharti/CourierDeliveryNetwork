﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class CustomerRegistrationModel
    {
        public Int64 Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string PostalCode { get; set; }
        public int SocialId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime DOB { get; set; }
        public string DeviceID { get; set; }
        public string UserToken { get; set; }
        public string StreetAddress { get; set; }
        public string LandMark { get; set; }

    }

    public class RegisterModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class RegisterModelObject
    {
        public RegisterModelResponse response { get; set; }
    }

    public class CustomerLoginModel
    {
        public int DeviceId { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceOsversion { get; set; }
        public string SocialId { get; set; }
        public string Email { get; set; }
       
       
    }

    public class CustomerLoginModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public Int64 UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public string UserToken { get; set; }
        public List<CustomerLoginModel> data { get; set; }
    }

    public class CustomerLoginModelObject
    {
        public CustomerLoginModelResponse response { get; set; }
    }

    public class CustomerAddressListModel
    {
        public string Status { get; set; }
        public Int64 Id { get; set; }
        public Int64 CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }
        public string Landmark { get; set; }
        public string City { get; set; }
        public string cityName { get; set; }
        public string Country { get; set; }
        public string countryName { get; set; }
        public string State { get; set; }
        public string stateName { get; set; }
        public string Pincode { get; set; }
        public int Address_No { get; set; }
        public bool WhetherDefault { get; set; }
        public Int64 Address_ID { get; set; }
        public string Mode { get; set; }
    }

    public class CustomerAddressListModelResponse
    {
        public CustomerAddressListModelResponse()
        {
            listdata = new List<CustomerAddressListModel>();
        }
        public string code { get; set; }
        public string message { get; set; }
        public List<CustomerAddressListModel> listdata { get; set; }
    }

    public class CustomerAddressListModelObject
    {
        public CustomerAddressListModelObject()
        {
            response = new CustomerAddressListModelResponse();
        }
        public CustomerAddressListModelResponse response { get; set; }
    }

    public class CustomerAdd_EditAddressModel
    {
        public Int64 Id { get; set; }
        public int Address_No { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }
        public int Country { get; set; }
        public int State { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public string Mode { get; set; }
    }

    public class CustomerAdd_EditAddressModelResponse
    {
        public CustomerAdd_EditAddressModelResponse()
        {
            customer_add_editlist = new List<CustomerAdd_EditAddressModel>();
        }
            
        public string code { get; set; }
        public string message { get; set; }
        public List<CustomerAdd_EditAddressModel> customer_add_editlist { get; set; }

    }
    public class CustomerAdd_EditAddressModelObject
    {
        public CustomerAdd_EditAddressModelObject()
        {
            response = new CustomerAdd_EditAddressModelResponse();
        }
        public CustomerAdd_EditAddressModelResponse response { get; set; }
    }

    public class CustomerMakeDefaultAddressModel
    {
        public int Address_No { get; set; }
        public int CustomerID { get; set; }
        public Int64 AddressID { get; set; }
    }
    public class CustomerMakeDefaultAddressModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class CustomerMakeDefaultAddressModelObject
    {
        public CustomerMakeDefaultAddressModelResponse response { get; set; }
    }


    public class RefreshTokenObject
    {
        public string code { get; set; }
        public string message { get; set; }
        public string userToken { get; set; }
    }

   

}