﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class OrderHistory
    {


         public class Currency
         {
           public string CurrencyCode { get; set; }
            public string CurrencyName { get; set; }

        }

         public class PickUp
         {
            public DateTime PickUpDate { get; set; }
            public DateTime PickUpTime { get; set; }
            public int PickUpAddress { get; set; }
        }

          public class DropOff
          {
            public int DropOffAddress { get; set; }
          }

                 public class CustomerOrderHistory
                {
          
                    public int ID { get; set; }
                    public int Status { get; set; }
                    public DateTime PickUpDate { get; set; }
                    public int Amount { get; set; }
                    public Currency currency { get; set; }
                    public PickUp pickUp { get; set; }
                    public DropOff dropOff { get; set; }
                    public CustomerOrderHistory()
                    {
                        dropOff = new DropOff();
                        pickUp = new PickUp();
                        currency = new Currency();
                    }
        }

            public class CustomerOrderHistoryResponse
            {
                   public string Status { get; set; }
                    public string code { get; set; }
                    public int offset { get; set; }
                    public int limit { get; set; }
                    public int total { get; set; }
                    public List<CustomerOrderHistory> order_history { get; set; }
            }

            public class ResponseMainModel
            {
                    public CustomerOrderHistoryResponse response { get; set; }
            public ResponseMainModel()
            {
                response = new CustomerOrderHistoryResponse();
            }
        }
            public class CustomerOrderHistoryRequest
            {
            public int Offset { get; set; }
            public int Limit { get; set; }
            public string SearchKey { get; set; }
            }


    }
}