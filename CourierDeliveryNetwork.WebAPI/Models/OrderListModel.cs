﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class OrderListModel
    {
       
            public class ProvidersDetails
            {
                public string CompanyLogo { get; set; }
                public string CompanyName { get; set; }
                public Int64 ProviderId { get; set; }
            }

            public class Currency
            {
                public string CurrencyCode { get; set; }
                public string CurrencyName { get; set; }
            }

            public class OrderDetails
            {
                public Int64 OrderID { get; set; }
                public int TotalPayable { get; set; }
                public Currency currency { get; set; }
            }

            public class PaymentDetails
            {

            public decimal ServiceTax { get; set; }
            public decimal VAT { get; set; }
            public decimal Discount { get; set; }
            public int TotalPayable { get; set; }
          
            }

            public class PickUp
            {
                public string StreetAddress { get; set; }
                public string Landmark { get; set; }
                public int Country { get; set; }
                public int State { get; set; }
                public string Pincode { get; set; }
            
            }

            public class DropOff
            {
                public string StreetAddress { get; set; }
                public string Landmark { get; set; }
                public int Country { get; set; }
                public int State { get; set; }
                public string Pincode { get; set; }
        }

            public class Dimensions
            {
                    public int Height { get; set; }
                    public int Width { get; set; }
                    public int Length { get; set; }
                    public int Weight { get; set; }
           }

            public class ParcelDetails
            {

          
            public int DeliveryType { get; set; }
            public int DocumentType { get; set; }
            public int ItemValue { get; set; }
            public string Description { get; set; }
            public Dimensions dimensions { get; set; }
            }

            public class OrderListMainModel
            {
                public ProvidersDetails providerDetails { get; set; }
                public OrderDetails orderDetails { get; set; }
                public PaymentDetails paymentDetails { get; set; }
                public PickUp pickUp { get; set; }
                public DropOff dropOff { get; set; }
                public ParcelDetails parcelDetails { get; set; }
            }

            public class OrderListMainModelResponse
        {
                public int code { get; set; }
                public List<OrderListMainModel> orderlist { get; set; }
                public string message { get; set; }
            }

            public class OrderListMainModelResponseObject
        {
                public Response response { get; set; }
            }
        }
    }
