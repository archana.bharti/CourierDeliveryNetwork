﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class PostBidModel
    {
        public class PostBidRequest
        {
            public int RequirementID { get; set; }
            public decimal Amount { get; set; }
            public string Description { get; set; }
            public Int64 CustomerID { get; set; }
            public string CurrencyCode { get; set; }

        }

        public class PostBidResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }

        public class PostBidModelObject
        {
            public PostBidResponse response { get; set; }
            public PostBidModelObject()
            {
                response = new PostBidResponse();
            }
        }
    }
}