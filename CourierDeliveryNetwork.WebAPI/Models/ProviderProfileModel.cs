﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class ProviderProfileModel
    {
        public class ProviderForgetPassword
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
        public class ProviderForgetPasswordResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }
        public class ProviderForgetPasswordObject
        {
            public ProviderForgetPasswordObject()
            {
                response = new ProviderForgetPasswordResponse();
            }
            public ProviderForgetPasswordResponse response { get; set; }
        }


        public class ProviderChangePasswordModel
        {
            public string Email { get; set; }
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class ProviderChangePasswordModelResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }
        public class ProviderChangePasswordModelObject
        {
            public ProviderChangePasswordModelObject()
            {
                response = new ProviderChangePasswordModelResponse();
            }
            public ProviderChangePasswordModelResponse response { get; set; }
        }
    }

    public class UserDetails
    {
        public string fName { get; set; }
        public string lName { get; set; }
        public string email { get; set; }
        public DateTime DOB { get; set; }
        public string phone { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public Int32 countryId { get; set; }
        public Int32 stateId { get; set; }
        public Int32 cityId { get; set; }
        public string pinCode { get; set; }
    }

    public class CompanyDetails
    {
        public string registrationNo { get; set; }
        public string name { get; set; }
        public string locality { get; set; }
        public string logo { get; set; }
        public string description { get; set; }
    }

    public class BankDetails
    {
        public string IFSCCode { get; set; }
        public string swiftCode { get; set; }
        public string accountType { get; set; }
        public string AccountNumber { get; set; }
        public string IBANNumber { get; set; }
        public string CINNumber { get; set; }
        public string payPalId { get; set; }
    }

    public class ServiceDetails
    {
        public string serviceType { get; set; }
        public string deliveryType { get; set; }
        public string servicesProvided { get; set; }
    }

    public class ProviderDetailsResponseModel
    {
        public int code { get; set; }
        public UserDetails userDetails { get; set; }
        public CompanyDetails companyDetails { get; set; }
        public BankDetails bankDetails { get; set; }
        public ServiceDetails serviceDetails { get; set; }

        public ProviderDetailsResponseModel()
        {
            companyDetails = new CompanyDetails();
            bankDetails = new BankDetails();
            serviceDetails = new ServiceDetails();
            userDetails = new UserDetails();
        }

    }

    public class ProviderResponseMainModel
    {
        public ProviderDetailsResponseModel response { get; set; }
        public ProviderResponseMainModel()
        {
            response = new ProviderDetailsResponseModel();
        }
    }


    public class Height
    {
    
        public int HeightFrom { get; set; }
        public int HeightTo { get; set; }
    }

    public class Weight
    {
        public int WeightFrom { get; set; }
        public int WeightTo { get; set; }
    }

    public class Width
    {
        public int WidthFrom { get; set; }
        public int WidthTo { get; set; }
    }

    public class Length
    {
        public int LengthFrom { get; set; }
        public int LengthTo { get; set; }
    }

    public class ProviderRatesModel
    {
        public Int64 id { get; set; }
        public string title { get; set; }
        public int DeliveryType { get; set; }
        public int DocumentType { get; set; }
        public Height height { get; set; }
        public Weight weight { get; set; }
        public Width width { get; set; }
        public Length length { get; set; }
        public int PricePerKG { get; set; }
        public Decimal MinimumCharges { get; set; }
        public int DeliveryDays { get; set; }

        public ProviderRatesModel()
        {
            height = new Height();
            weight = new Weight();
            width = new Width();
            length = new Length();
        }
    }

    public class ProviderRatesModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ProviderRatesModelObject
    {
        public ProviderRatesModelResponse response { get; set; }
    }

    public class RateResponse
    {
        public RateResponse()
        {
            rates = new List<ProviderRatesModel>();
        }

        public string code { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public int totalRecords { get; set; }
        public List<ProviderRatesModel> rates { get; set; }
    }

    public class RateObject
    {
        public RateObject()
        {
            response = new RateResponse();
        }
        public RateResponse response { get; set; }
    }

    public class EditProviderDetailRespionse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class EditProviderDetailObject
    {
        public EditProviderDetailObject()
        {
            response = new EditProviderDetailRespionse();
        }
        public EditProviderDetailRespionse response { get; set; }
    }

    public class EditProviderCompanyRespionse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class EditProviderCompanyDetailObject
    {
        public EditProviderCompanyDetailObject()
        {
            response = new EditProviderCompanyRespionse();
        }
        public EditProviderCompanyRespionse response { get; set; }
    }

    public class OperatingCountryModel
    {
        public string CountryName { get; set; }
        public string Flag { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
    }

    public class MainOperatingCountryResponse
    {
        public MainOperatingCountryResponse()
        {
            countries = new List<OperatingCountryModel>();
        }
        public string code { get; set; }
        public string message { get; set; }
        public List<OperatingCountryModel> countries { get; set; }
    }

    public class OperatingCountryResponse
    {
        public OperatingCountryResponse()
        {
            opContresponse = new MainOperatingCountryResponse();
        }
        public MainOperatingCountryResponse opContresponse { get; set; }
    }

    public class EditOperatingCountries
    {
        public List<int> countryId { get; set; }
    }

    public class EditStausRequest
    {
        public string id { get; set; }
        public string status { get; set; }
    }

    public class EditStausResponseModel
    {
        public string code { get; set; }
        public string message { get; set; }
    }
    public class EditStausResponse
    {
        public EditStausResponseModel response { get; set; }
    }

    public class ServiceDeliveryRequest
    {
        public string documentType { get; set; }
        public string priorityType { get; set; }
        public string deliveryType { get; set; }
    }
    public class ServiceDeliveryResponseModel
    {
        public string code { get; set; }
        public string message { get; set; }
    }
   
    public class PaymentDetailsRequest
    {
        public string IFSC { get; set; }
        public string SwiftCode { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string IBAN { get; set; }
        public string CIN { get; set; }
        public string PayPALID { get; set; }
    }

    public class PaymentDetailsResponseModel
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}