﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class ProviderRegistrationModel
    {
        public Int64 ProviderId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }

        public string Password { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string PostalCode { get; set; }
      
        public string Address { get; set; }
        public int ServicesOffered { get; set; }
        public int ServicesLocation { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyName { get; set; }
        public string Locality { get; set; }
        public string CompanyLogo { get; set; }
        public string Description { get; set; }
        public string ProviderToken { get; set; }
    }

    public class ProviderRegisterModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ProviderRegisterModelObject
    {
        public RegisterModelResponse response { get; set; }
    }

    public class ProviderLoginModel
    {
        public int DeviceId { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceOsversion { get; set; }
        public string SocialId { get; set; }
        public string Email { get; set; }
       
       
    }

    public class ProviderLoginModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public Int64 ProviderId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public string ProviderToken { get; set; }
        public List<ProviderLoginModel> data { get; set; }
    }

    public class ProviderLoginModelObject
    {
        public ProviderLoginModelResponse response { get; set; }
    }

    public class ProviderAddressListModel
    {
        public string Status { get; set; }
        public Int64 Id { get; set; }
        public Int64 CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }
        public string Landmark { get; set; }
        public string City { get; set; }
        public string cityName { get; set; }
        public string Country { get; set; }
        public string countryName { get; set; }
        public string State { get; set; }
        public string stateName { get; set; }
        public string Pincode { get; set; }
        public int Address_No { get; set; }
        public bool WhetherDefault { get; set; }
        public Int64 Address_ID { get; set; }
        public string Mode { get; set; }
    }

    public class ProviderAddressListModelResponse
    {
        public ProviderAddressListModelResponse()
        {
            listdata = new List<ProviderAddressListModel>();
        }
        public string code { get; set; }
        public string message { get; set; }
        public List<ProviderAddressListModel> listdata { get; set; }
    }

    public class ProviderAddressListModelObject
    {
        public ProviderAddressListModelObject()
        {
            response = new ProviderAddressListModelResponse();
        }
        public ProviderAddressListModelResponse response { get; set; }
    }

    public class ProviderAdd_EditAddressModel
    {
        public Int64 Id { get; set; }
        public int Address_No { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }

        public int Country { get; set; }
        public int State { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public string Mode { get; set; }
    }

    public class ProviderAdd_EditAddressModelResponse
    {
        public ProviderAdd_EditAddressModelResponse()
        {
            Provider_add_editlist = new List<ProviderAdd_EditAddressModel>();
        }
            
        public string code { get; set; }
        public string message { get; set; }
        public List<ProviderAdd_EditAddressModel> Provider_add_editlist { get; set; }

    }
    public class ProviderAdd_EditAddressModelObject
    {
        public ProviderAdd_EditAddressModelObject()
        {
            response = new ProviderAdd_EditAddressModelResponse();
        }
        public ProviderAdd_EditAddressModelResponse response { get; set; }
    }

    public class ProviderMakeDefaultAddressModel
    {
        public int Address_No { get; set; }
        public int CustomerID { get; set; }
        public Int64 AddressID { get; set; }
    }
    public class ProviderMakeDefaultAddressModelResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ProviderMakeDefaultAddressModelObject
    {   
        public ProviderMakeDefaultAddressModelResponse response { get; set; }
    }
}