﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class ProvidersOrderHistoryModel
    {
        public class ProvidersOrderHistoryData
        {
            public Int64 ProviderId { get; set; }
            public int Status { get; set; }
            public int TotalPayable { get; set; }
            public Int64 OrderID { get; set; }
            public string ClientName { get; set; }
        }

        public class ProvidersOrderHistoryResponse
        {
            public string code { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
            public List<ProvidersOrderHistoryData> providersOrder_history { get; set; }
            public ProvidersOrderHistoryResponse()
            {
                providersOrder_history = new List<ProvidersOrderHistoryData>();
            }
        }

        public class ProvidersOrderHistoryObject
        {
            public ProvidersOrderHistoryResponse response { get; set; }
            public ProvidersOrderHistoryObject()
            {
                response = new ProvidersOrderHistoryResponse();
            }
        }
    }
}