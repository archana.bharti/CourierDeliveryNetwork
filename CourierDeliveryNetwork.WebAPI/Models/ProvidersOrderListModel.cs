﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class ProvidersOrderListModel
    {
        public class CustomerDetails
        {
            public Int64 UserId { get; set; }
            public string Name { get; set; }
            public string PhoneNo { get; set; }
            public string Email { get; set; }
        }

        public class Currency
        {
            public string CurrencyCode { get; set; }
            public string CurrencyName { get; set; }
        }

        public class OrderDetails
        {
            public Int64 OrderID { get; set; }
            public int TotalPayable { get; set; }
            public Currency currency { get; set; }
            public string Status { get; set; }
            public OrderDetails()
            {
                currency = new Currency();
            }  
        }

        public class PaymentDetails
        {
            public decimal ServiceTax { get; set; }
            public decimal VAT { get; set; }
            public decimal Discount { get; set; }
            public int TotalPayable { get; set; }
            public int Status { get; set; }       
        }

        public class PickUp
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }
        }

        public class DropOff
        {
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public int Country { get; set; }
            public int State { get; set; }
            public string Pincode { get; set; }
        }

        public class ProviderOrderListData
        {
            public Int64 ProviderID { get; set; }
            public CustomerDetails customerDetails { get; set; }
            public string Title { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
            public PickUp pickUp { get; set; }
            public DropOff dropOff { get; set; }

            public ProviderOrderListData()
            {
                customerDetails = new CustomerDetails();
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
                pickUp = new PickUp();
                dropOff = new DropOff();
            }
        }

        public class Filter
        {
            public DateTime datefrom { get; set; }
            public DateTime dateto { get; set; }
        }

        public class ProviderOrderListResponse
        {
            public int code { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }          
            public Filter filter { get; set; }

            public List<ProviderOrderListData> providerOrder_List { get; set; }

            public ProviderOrderListResponse()
            {
                filter = new Filter();
                providerOrder_List = new List<ProviderOrderListData>();
            }
        }

        public class ProviderOrderListObject
        {
            public ProviderOrderListResponse response { get; set; }
            
            public ProviderOrderListObject()
            {
                response = new ProviderOrderListResponse();
            }
        }
    }
}