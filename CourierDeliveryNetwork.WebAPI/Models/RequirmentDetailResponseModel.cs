﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class RequirmentDetailResponseModel
    {
       public RequirmentDetailResponseModel()
        {
            response = new RequirementDetailResponse();
        }
        public RequirementDetailResponse response { get; set; }
    }

    //public class Currency
    //{
    //    public string name { get; set; }
    //    public string id { get; set; }
    //}

    public class RequirmentResponseData
    {
        
        public RequirmentResponseData()
        {
            ThumbUrl = new List<string>();
            ImageUrl = new List<string>();
        }
        public Int32 RequirementID { get; set; }
        public string Title { get; set; }
        public string postDate { get; set; }
        public DateTime ValidTill { get; set; }
        public Decimal MinimumAmount { get; set; }

        public string Category { get; set; }
        public string CurrencyName { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string numberOfBids { get; set; }
        public List<string> ThumbUrl { get; set; }
        public List<string> ImageUrl { get; set; }
    }

    public class RequirementDetailResponse
    {
     public   RequirementDetailResponse()
        {
            data = new RequirmentResponseData();
        }
        public string code { get; set; }
        public RequirmentResponseData data { get; set; }
    }
}