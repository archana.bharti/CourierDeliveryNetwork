﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class RequirmentListResponse
    {
        public ListResponse response { get; set; }
    }
        public class ResponseData
        {
        public ResponseData()
        {
            thumbURL = new List<string>();
            imagesURL = new List<string>();
        }
            public int RequirementID { get; set; }
            public string tittle { get; set; }
            public string date { get; set; }
            public string amount { get; set; }
            public string status { get; set; }
            public string description { get; set; }
            public string CurrencyName { get; set; }
            public string numberOfBids { get; set; }
            public List<string> thumbURL { get; set; }
            public List<string> imagesURL { get; set; }
        }

        public class ListResponse
        {
        public ListResponse()
        {
            data = new List<ResponseData>();
        }
            public string code { get; set; }
            public string offset { get; set; }
            public string limit { get; set; }
            public string total { get; set; }
            public List<ResponseData> data { get; set; }
        }

      
}