﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class StateModel
    {
        public string StateName { get; set; }
        public int StateId { get; set; }
       
    }

    public class StateResponse
    {
        public StateResponse()
        {
            states = new List<StateModel>();
        }
        public int CountryId { get; set; }
        public string code { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public string SearchKey { get; set; }

        public int totalRecords { get; set; }
        public List<StateModel> states { get; set; }
    }

    public class StateObject
    {
        public StateObject()
        {
            response = new StateResponse();
        }
        public StateResponse response { get; set; }
        //public int Offset { get; set; }
        //public int Limit { get; set; }
        //public int CountryId { get; set; }
    }


    public class CityModel
    {
        public string CityName { get; set; }
        public int cityid { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
    }

    public class CityModelResponse
    {
        public CityModelResponse()
        {
            Cities = new List<CityModel>();
        }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string code { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public string SearchKey { get; set; }
       
        public List<CityModel> Cities { get; set; }
    }

    public class CityModelObject
    {
        public CityModelObject()
        {
            response = new CityModelResponse();
        }
        public CityModelResponse response { get; set; }
    }

}