﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class Token
    {
        public static string GetUniqueToken()
        {
            char[] AvailableCharacters = {
                                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                                        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_', '!',
                                        '#', '@', '$', '%', '^', '&', '*', '(', ')', '/', '+', ';', '~'
                                                    };

            char[] identifier = new char[20];
            byte[] randomData = new byte[20];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomData);
            }
            for (int idx = 0; idx < identifier.Length; idx++)
            {
                int pos = randomData[idx] % AvailableCharacters.Length;
                identifier[idx] = AvailableCharacters[pos];
            }

            StringBuilder builder = new StringBuilder();
            foreach (char value in identifier)
            {
                builder.Append(value);
            }
            return builder.ToString();
        }
    }
}