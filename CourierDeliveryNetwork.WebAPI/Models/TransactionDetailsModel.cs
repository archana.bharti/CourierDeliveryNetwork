﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class TransactionDetailsModel
    {
        public class TransactionDetailsRequestDate
        {
            public DateTime from { get; set; }
            public DateTime to { get; set; }
        }

        public class TransactionRequestModel
        {
            public TransactionRequestModel()
            {
                requestdate = new TransactionDetailsRequestDate();
            }
            public TransactionDetailsRequestDate requestdate { get; set; }
            public Int64 offset { get; set; }
            public Int64 limit { get; set; }
        }

        public class Customer
        {
            public string name { get; set; }
            public string address { get; set; }
            public string email { get; set; }
        }

        public class PickUp
        {
            public string houseStreet { get; set; }
            public string landMark { get; set; }
            public Int32 countryId { get; set; }
            public Int32 stateId { get; set; }
            public string postalCode { get; set; }
        }

        public class DropOff
        {
            public string houseStreet { get; set; }
            public string landMark { get; set; }
            public Int32 countryId { get; set; }
            public Int32 stateId { get; set; }
            public string postalCode { get; set; }
        }

        public class OrderDetails
        {
            public Int64 orderId { get; set; }
            public int amountTobePaid { get; set; }
            public string status { get; set; }
        }

        public class PaymentDetails
        {
            public Int64? transactionId { get; set; }
            public decimal serviceTax { get; set; }
            public decimal vat { get; set; }
            public decimal discount { get; set; }
            public int totalPayble { get; set; }
            public string status { get; set; }
        }

        public class Transaction
        {
            public Transaction()
            {
                customer = new Customer();
                pickUp = new PickUp();
                dropOff = new DropOff();
                orderDetails = new OrderDetails();
                paymentDetails = new PaymentDetails();
            }
            public Customer customer { get; set; }
            public PickUp pickUp { get; set; }
            public DropOff dropOff { get; set; }
            public string description { get; set; }
            public OrderDetails orderDetails { get; set; }
            public PaymentDetails paymentDetails { get; set; }
        }

        public class TransactionResponse
        {
            public TransactionResponse()
                {
                data = new List<Transaction>();
                }
            public int code { get; set; }
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
            public List<Transaction> data { get; set; }
        }

        public class MainTransactionResponseObject
        {
            public MainTransactionResponseObject()
            {
                response = new TransactionResponse();
            }
            public TransactionResponse response { get; set; }
        }
    }
}