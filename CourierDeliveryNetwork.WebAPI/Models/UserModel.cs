﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CourierDeliveryNetwork.WebAPI.Models
{
    public class UserModel
    {
        public class UserForgetPassword
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
        public class UserForgetPasswordResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }
        public class UserForgetPasswordObject
        {
            public UserForgetPasswordResponse response { get; set; }
            public UserForgetPasswordObject()
            {
                response = new UserForgetPasswordResponse();
            }
        }

        public class UserContactUs
        {
            public int EnquiryID { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string ContactNo { get; }
            public DateTime Date { get; set; }
            public string Status { get; set; }
            public string Message { get; set; }
        }
        public class UserContactUsResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }

        public class UserContactUsResponseObject
        {
            public UserContactUsResponse response { get; set; }
        }


        public class UserCategories
        {
            public int CategoryID { get; set; }
            public string Category { get; set; }
        }

        public class UserCategoriesResponse
        {
            public UserCategoriesResponse()
            {
                categories = new List<UserCategories>();
            }
            public string code { get; set; }
            public List<UserCategories> categories { get; set; }
        }

        public class UserCategoriesRootObject
        {
            public UserCategoriesRootObject()
            {
                response = new UserCategoriesResponse();
            }
            public UserCategoriesResponse response { get; set; }
        }


        public class UserDetail
        {
            public Int64 UserId { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string StreetAddress { get; set; }
            public string Landmark { get; set; }
            public string PostalCode { get; set; }
            public int StateId { get; set; } 
            public int CountryId { get; set; }
          
        }

        public class UserDetailResponse
        {
            public UserDetailResponse()
            {
                userdetail = new List<UserDetail>();
            }
            public string code { get; set; }
            public List<UserDetail> userdetail { get; set; }
        }

        public class UserDetailObject
        {
            public UserDetailObject()
            {
                response = new UserDetailResponse();
            }
            public UserDetailResponse response { get; set; }
           
        }

        public class EditUserDetailRespionse
        {

            public string code { get; set; }
            public string message { get; set; }
        }

        public class EditUserDetailObject
        {
            public EditUserDetailObject()
            {
                response = new EditUserDetailRespionse();
            }
            public EditUserDetailRespionse response { get; set; }
        }
        public partial class UserTokenModel
        {
            public long Id { get; set; }
            public Nullable<long> UserId { get; set; }
          
            public string Token { get; set; }
           
        }

        public class UserChangePasswordModel
        {
            public string Email { get; set; }
            public string Password { get; set; }
           
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class UserChangePasswordModelResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }
        public class UserChangePasswordModelObject
        {
            public UserChangePasswordModelResponse response { get; set; }
        }

        public class ContactInfoModel
        {
            public string Email { get; set; }
            public string Name { get; set; }
            public string ContactNo { get; set; }
        }

        public class ContactInfoModelResponse
        {
            public ContactInfoModelResponse()
            {
                contactinfolist = new List<ContactInfoModel>();
            }
            public string code { get; set; }
            public List<ContactInfoModel> contactinfolist { get; set; }
            public string message { get; set; }
        }

        public class ContactInfoModelObject
        {
            public ContactInfoModelObject()
            {
                response = new ContactInfoModelResponse();
            }
            public ContactInfoModelResponse response { get; set; }
        }
    }
}